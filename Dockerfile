FROM node:18.20.5 as builder

WORKDIR /app/
COPY package.json .
RUN yarn install
COPY . .
RUN yarn build

FROM nginx:1.24-bullseye
COPY --from=builder /app/build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY nginx/gzip.conf /etc/nginx/conf.d/gzip.conf

WORKDIR /usr/share/nginx/html
RUN chown -R :www-data /usr/share/nginx/html

COPY ./env.sh .
RUN chmod +x env.sh

COPY .env.production .env
RUN ./env.sh -e .env -o ./

CMD ["nginx", "-g", "daemon off;"]
