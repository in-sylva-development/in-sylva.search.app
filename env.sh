#!/bin/bash

usage() {
    BASENAME=$(basename "$0")
    echo "Usage: $BASENAME -e <env-file> -o <output-file>"
    echo "  -e, --env-file     Path to .env file"
    echo "  -o, --output-file  Path to output '.env-config.js' file"
    exit 1
}

while getopts "e:o:" opt; do
    case $opt in
    e)
        ENV_FILE=$OPTARG
        ;;
    o)
        OUT_DIRECTORY=$OPTARG
        ;;
    \?)
        # Invalid option
        echo "Invalid option: -$OPTARG"
        usage
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument."
        usage
        exit 1
        ;;
    esac
done

# Check if required options are provided
if [ -z "$OUT_DIRECTORY" ]; then
    echo "Error: -o (output directory) is required."
    echo ""
    usage
    exit 1
fi

if [ -z "$ENV_FILE" ]; then
    echo "Error: -e (environment file) is required."
    echo ""
    usage
    exit 1
fi

ENV_FILE_PATH=$(realpath $ENV_FILE)

# Check if the environment file exists
if [[ ! -f $ENV_FILE_PATH ]]; then
    echo "Environment file does not exist"
    echo ""
    usage
    exit 1
fi

# Check if the environment file is readable
if [[ ! -r $ENV_FILE_PATH ]]; then
    echo "Environment file is not readable"
    echo ""
    usage
    exit 1
fi

# Check if the environment file is empty
if [[ ! -s $ENV_FILE_PATH ]]; then
    echo "Environment file is empty"
    echo ""
    usage
    exit 1
fi

# Check if the environment file has the correct format
BAD_LINES=$(grep -v -P '^[^=\s]+=[^=\s]+$' "$ENV_FILE_PATH")
if [ -n "$BAD_LINES" ]; then
    echo "Environment file has incorrect format or contains empty values:"
    echo "$BAD_LINES"
    echo ""
    usage
    exit 1
fi

FULL_PATH_OUTPUT_DIRECTORY=$(realpath $OUT_DIRECTORY)

# Check if the output directory is a directory, exists, and can be written
if [[ ! -d $FULL_PATH_OUTPUT_DIRECTORY ]]; then
    echo "Output directory does not exist or is not a directory"
    echo ""
    usage
    exit 1
fi

if [[ ! -w $FULL_PATH_OUTPUT_DIRECTORY ]]; then
    echo "Output directory is not writable"
    echo ""
    usage
    exit 1
fi

OUTPUT_FILE="env-config.js"
FULL_OUTPUT_PATH="$FULL_PATH_OUTPUT_DIRECTORY/$OUTPUT_FILE"

# Remove the output file if it exists
if [[ -f $FULL_OUTPUT_PATH ]]; then
    rm $FULL_OUTPUT_PATH
fi

# Add assignment
echo "window._env_ = {" >>$FULL_OUTPUT_PATH

while read -r line || [[ -n "$line" ]]; do
    # Split env variables by '='
    varname=$(echo $line | cut -d'=' -f1)
    value=$(echo $line | cut -d'=' -f2)

    # Append configuration property to JS file
    echo "  $varname: \"$value\"," >>$FULL_OUTPUT_PATH
done <$ENV_FILE_PATH

echo "}" >>$FULL_OUTPUT_PATH
