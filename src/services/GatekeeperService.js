export class InSylvaGatekeeperClient {
  constructor(getUserInfo) {
    this.getUserInfo = getUserInfo;
  }
  async get(path, payload) {
    const userInfo = await this.getUserInfo();
    const { access_token } = userInfo;
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    };
    const response = await fetch(
      `${process.env.REACT_APP_IN_SYLVA_GATEKEEPER_BASE_URL}${path}`,
      {
        method: 'GET',
        headers,
        mode: 'cors',
        body: JSON.stringify(payload),
      }
    );
    return await response.json();
  }

  async post(path, requestContent) {
    const userInfo = await this.getUserInfo();
    const { access_token } = userInfo;
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    };
    const response = await fetch(
      `${process.env.REACT_APP_IN_SYLVA_GATEKEEPER_BASE_URL}${path}`,
      {
        method: 'POST',
        headers,
        body: JSON.stringify(requestContent),
        mode: 'cors',
      }
    );
    return await response.json();
  }

  async delete(path, requestContent) {
    const userInfo = await this.getUserInfo();
    const { access_token } = userInfo;
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    };
    const response = await fetch(
      `${process.env.REACT_APP_IN_SYLVA_GATEKEEPER_BASE_URL}${path}`,
      {
        method: 'DELETE',
        headers,
        body: JSON.stringify(requestContent),
        mode: 'cors',
      }
    );
    if (response.status === 204) {
      return {
        success: true,
        message: 'Request deleted successfully',
      };
    } else {
      return {
        success: false,
        message: 'Request not deleted',
      };
    }
  }

  async createUser(sub, email) {
    const path = `/users`;
    return await this.post(path, {
      kc_id: sub,
      email,
    });
  }

  async findUserBySub(sub) {
    const path = `/users/${sub}`;
    return await this.get(path);
  }

  async getRoles() {
    const path = `/roles`;
    return await this.get(path);
  }

  async createUserRequest(message, sub) {
    const path = `/users/${sub}/requests`;
    return await this.post(path, {
      message: message,
    });
  }

  async deleteUserRequest(id) {
    const path = `/user-requests/${id}`;
    return await this.delete(path, {});
  }

  async getGroups() {
    const path = `/groups`;
    return await this.get(path);
  }

  async getPublicFields() {
    const path = `/public_std_fields`;
    return await this.get(path);
  }

  async getUserFieldsDisplaySettings(sub) {
    const path = `/users/${sub}/fields`;
    return await this.get(path);
  }

  async setUserFieldsDisplaySettings(sub, fields) {
    const path = `/users/${sub}/fields`;
    return await this.post(path, {
      fields_id: fields,
    });
  }

  async getSources() {
    const path = `/sources`;
    return await this.get(path);
  }

  async getUserSources(sub) {
    const path = `/users/${sub}/sources`;
    return await this.get(path);
  }

  async searchQuery(payload) {
    const path = `/search`;
    return await this.post(path, payload);
  }

  async getUserHistory(sub) {
    const path = `/users/${sub}/history`;
    return await this.get(path);
  }

  async addHistory(sub, payload) {
    const path = `/users/${sub}/history`;
    return await this.post(path, payload);
  }
}
