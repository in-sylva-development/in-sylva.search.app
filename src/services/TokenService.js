export default class TokenService {
  async getUserInfo(access_token) {
    const issuerUrl = process.env.REACT_APP_KEYCLOAK_BASE_URL;
    const userInfoEndpoint = issuerUrl + '/protocol/openid-connect/userinfo';

    const response = await fetch(userInfoEndpoint, {
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    });
    if (response.status !== 200) {
      return null;
    } else {
      const userInfo = await response.json();
      return userInfo;
    }
  }

  async refreshToken(refresh_token) {
    const issuerUrl = process.env.REACT_APP_KEYCLOAK_BASE_URL;
    const tokenEndpoint = issuerUrl + '/protocol/openid-connect/token';

    const response = await fetch(tokenEndpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `grant_type=refresh_token&refresh_token=${refresh_token}&client_id=${process.env.REACT_APP_KEYCLOAK_CLIENT_ID}&client_secret=${process.env.REACT_APP_KEYCLOAK_CLIENT_SECRET}`,
    });

    if (response.status !== 200) {
      return null;
    } else {
      const response = await response.json();
      return response;
    }
  }
}
