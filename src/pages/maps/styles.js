import { Fill, Stroke, Style } from 'ol/style';

const styles = {
  filterLayer: new Style({
    fill: new Fill({
      color: 'rgba(128, 128, 128, 0.25)',
    }),
    stroke: new Stroke({
      width: 1,
      lineDash: [4],
      color: 'rgba(0, 0, 0, 0.5)',
    }),
  }),
  selectTool: new Style({
    fill: new Fill({
      color: 'rgba(80, 200, 120, 0.6)',
    }),
    stroke: new Stroke({
      color: 'rgba(255, 255, 255, 0.7)',
      width: 2,
    }),
  }),
  container: {
    minWidth: '70vw',
    maxWidth: '70vw',
  },
  mapContainer: {
    minWidth: '70vw',
    minHeight: '58vh',
    maxHeight: '58vh',
  },
  selectedPointsList: {
    overflow: 'auto',
    maxHeight: '72vh',
  },
  layersTable: {
    width: '70vw',
    cursor: 'pointer',
    marginTop: '10px',
  },
  layersTableCells: {
    padding: '10px',
  },
  filtersSelect: {
    maxWidth: '50%',
  },
  unselectAllButton: {
    minWidth: '100%',
  },
};

export default styles;
