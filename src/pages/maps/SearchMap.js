import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Map, View } from 'ol';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import { Fill, Stroke, Style, Text } from 'ol/style';
import SourceOSM from 'ol/source/OSM';
import BingMaps from 'ol/source/BingMaps';
import { Vector as VectorSource } from 'ol/source';
import WMTS from 'ol/source/WMTS';
import WMTSTileGrid from 'ol/tilegrid/WMTS';
import { getWidth } from 'ol/extent';
import { platformModifierKeyOnly } from 'ol/events/condition';
import GeoJSON from 'ol/format/GeoJSON';
import { defaults, DragBox, MouseWheelZoom, Select } from 'ol/interaction';
import { Circle, Point } from 'ol/geom';
import Feature from 'ol/Feature';
import {
  defaults as defaultControls,
  FullScreen,
  OverviewMap,
  ScaleLine,
  Zoom,
} from 'ol/control';
import 'ol/ol.css';
import {
  EuiButton,
  EuiButtonGroup,
  EuiCheckbox,
  EuiComboBox,
  EuiProgress,
  EuiSpacer,
  EuiTitle,
  EuiPanel,
  EuiFlexGroup,
  EuiFlexItem,
  EuiButtonIcon,
  EuiToolTip,
  htmlIdGenerator,
} from '@elastic/eui';
import { updateArrayElement } from '../../Utils.js';
import { useTranslation } from 'react-i18next';
import styles from './styles.js';
import * as proj from 'ol/proj';
import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';

const proj3857 = proj.get('EPSG:3857');
proj4.defs(
  'EPSG:2154',
  '+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
);
register(proj4);

const initResolutions = () => {
  const resolutions = [];
  const maxResolution = getWidth(proj3857.getExtent()) / 256;
  for (let i = 0; i < 20; i++) {
    resolutions[i] = maxResolution / Math.pow(2, i);
  }
  return resolutions;
};

const initMatrixIds = () => {
  const matrixIds = [];
  for (let i = 0; i < 20; i++) {
    matrixIds[i] = i.toString();
  }
  return matrixIds;
};

const pointBaseStyle = new Style({
  fill: new Fill({
    color: 'rgba(80, 200, 120, 0.6)',
  }),
  stroke: new Stroke({
    color: 'rgba(80, 200, 120, 0.8)',
    width: 2,
  }),
  text: new Text({
    textAlign: 'center',
    textBaseline: 'middle',
    font: '12px Arial',
    fill: new Fill({
      color: 'rgba(0, 0, 0, 1)',
    }),
    stroke: new Stroke({
      color: 'rgba(255, 255, 255, 1)',
      width: 1,
    }),
    offsetX: 0,
    offsetY: 0,
    rotation: 0,
  }),
});

const SearchMap = ({
  searchResults,
  selectedPointsIds,
  setSelectedPointsIds,
  setResourceFlyoutDataFromId,
  setIsResourceFlyoutOpen,
}) => {
  const { t } = useTranslation('maps');
  // ref for handling zoomHelperText display
  const timerRef = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [center, setCenter] = useState(proj.fromLonLat([2.5, 46.5]));
  const [zoom, setZoom] = useState(6);
  // Selection tool mode: false = select points ; true = unselect points.
  const [selectionToolMode, setSelectionToolMode] = useState(false);
  const filterOptions = [
    { label: t('maps:layersTable.queryResults'), value: 'ResRequete' },
    { label: t('maps:layersTable.regions'), value: 'regions' },
    { label: t('maps:layersTable.departments'), value: 'departments' },
    { label: t('maps:layersTable.sylvoEcoRegions'), value: 'sylvoEcoRegions' },
  ];
  const [selectedFilterOptions, setSelectedFilterOptions] = useState([filterOptions[0]]);
  const [zoomHelperTextFeature, setZoomHelperTextFeature] = useState(
    new Feature({
      geometry: new Point(center),
    })
  );

  const pointStyle = (feature, resolution) => {
    // Change color if point is selected
    if (feature.get('isSelected')) {
      pointBaseStyle.getFill().setColor('rgba(120, 80, 200, 0.6)');
      pointBaseStyle.getStroke().setColor('rgba(120, 80, 200, 0.8)');
    } else {
      pointBaseStyle.getFill().setColor('rgba(80, 200, 120, 0.6)');
      pointBaseStyle.getStroke().setColor('rgba(80, 200, 120, 0.8)');
    }
    // Display label text if map is enough zoomed in
    const zoomLevel = map.getView().getZoomForResolution(resolution);
    const label = zoomLevel >= 9 ? feature.get('nom') : null;
    pointBaseStyle.getText().setText(label);
    return pointBaseStyle;
  };

  const mapFilters = {
    ResRequete: new VectorLayer({
      name: 'ResRequete',
      source: new VectorSource({}),
    }),
    selectedPointsLayer: new VectorLayer({
      name: 'selectedPointsSource',
      source: new VectorSource({}),
      visible: true,
    }),
    regions: new VectorLayer({
      name: 'regions',
      source: new VectorSource({
        url: 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ADMINEXPRESS-COG-CARTO.LATEST:region&outputFormat=application/json',
        format: new GeoJSON(),
      }),
      style: styles.filterLayer,
    }),
    departments: new VectorLayer({
      name: 'departments',
      source: new VectorSource({
        url: 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ADMINEXPRESS-COG-CARTO.LATEST:departement&outputFormat=application/json',
        format: new GeoJSON(),
      }),
      style: styles.filterLayer,
    }),
    sylvoEcoRegions: new VectorLayer({
      name: 'sylvoEcoRegions',
      source: new VectorSource({
        url: 'https://geodata.inrae.fr/geoserver/inrae/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAMES=inrae%3Aser_l93&OUTPUTFORMAT=application%2Fjson',
        format: new GeoJSON(),
      }),
      style: styles.filterLayer,
    }),
  };
  const sourceOSM = new SourceOSM();
  const [mapLayers, setMapLayers] = useState([
    new TileLayer({
      name: 'osm-layer',
      visible: true,
      source: sourceOSM,
    }),
    new TileLayer({
      name: 'Bing Aerial',
      visible: false,
      preload: Infinity,
      source: new BingMaps({
        key: 'AtdZQap9X-lowJjvdPhTgr1BctJuGGm-ZoVw9wO6dHt1VDURjRKEkssetwOe31Xt',
        imagerySet: 'Aerial',
      }),
    }),
    new TileLayer({
      name: 'IGN',
      visible: false,
      source: new WMTS({
        url: 'https://data.geopf.fr/wmts',
        layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
        matrixSet: 'PM',
        format: 'image/png',
        projection: proj3857,
        tileGrid: new WMTSTileGrid({
          origin: [-20037508, 20037508],
          resolutions: initResolutions(),
          matrixIds: initMatrixIds(),
        }),
        style: 'normal',
        attributions:
          '<a href="https://www.ign.fr/" target="_blank">' +
          '<img src="https://www.ign.fr/files/default/styles/thumbnail/public/2020-06/logoIGN_300x200.png?itok=V80_0fm-" title="Institut national de l\'information géographique et forestière" alt="IGN"></a>',
      }),
    }),
    new VectorLayer({
      name: 'queryResults',
      visible: true,
      source: new VectorSource({}),
    }),
    mapFilters['selectedPointsLayer'],
    new VectorLayer({
      name: 'zoomHelperText',
      visible: false,
      background: 'rgba(0, 0, 0, 0.5)',
      source: new VectorSource({
        features: [zoomHelperTextFeature],
      }),
    }),
  ]);

  const getLayerIndex = useCallback(
    (name) => {
      let index = 0;
      mapLayers.forEach((layer) => {
        if (layer.get('name') === name) {
          index = mapLayers.indexOf(layer);
        }
      });
      return index;
    },
    [mapLayers]
  );

  // Two layers are set to visible on page load
  const initMapLayersVisibility = () => {
    let initialMapLayersVisibility = new Array(mapLayers.length).fill(false);
    initialMapLayersVisibility[getLayerIndex('osm-layer')] = true;
    initialMapLayersVisibility[getLayerIndex('selectedPointsSource')] = true;
    initialMapLayersVisibility[getLayerIndex('queryResults')] = true;
    return initialMapLayersVisibility;
  };
  const [mapLayersVisibility, setMapLayersVisibility] = useState(
    initMapLayersVisibility()
  );

  const [map] = useState(
    new Map({
      target: null,
      layers: mapLayers,
      interactions: defaults({ mouseWheelZoom: false }).extend([
        new MouseWheelZoom({
          condition: platformModifierKeyOnly,
        }),
      ]),
      controls: defaultControls({
        zoom: false,
      }).extend([
        new FullScreen(),
        new Zoom(),
        new ScaleLine(),
        new OverviewMap({
          layers: [
            new TileLayer({
              source: sourceOSM,
            }),
          ],
        }),
      ]),
      view: new View({
        center: center,
        zoom: zoom,
      }),
    })
  );

  useEffect(() => {
    const zoomHelperTextStyle = new Style({
      text: new Text({
        font: '24px Arial',
        text: t('maps:layersTable.zoomHelperText'),
        fill: new Fill({
          color: 'white',
        }),
      }),
    });
    let tmpZoomHelperTextFeature = new Feature({
      geometry: new Point(center),
    });
    tmpZoomHelperTextFeature.setStyle(zoomHelperTextStyle);
    setZoomHelperTextFeature(tmpZoomHelperTextFeature);
  }, [t, center]);

  useEffect(() => {
    const zoomHelperTextSource = map
      .getLayers()
      .item(getLayerIndex('zoomHelperText'))
      .getSource();
    zoomHelperTextSource.clear();
    zoomHelperTextSource.addFeature(zoomHelperTextFeature);
  }, [zoomHelperTextFeature]);

  // a select interaction object to handle click
  const select = new Select({
    style: styles.selectTool,
  });
  const [selectedFeatures, setSelectedFeatures] = useState(select.getFeatures());
  const [polygonSource, setPolygonSource] = useState(new VectorSource({}));
  const [dragBox, setDragBox] = useState(
    new DragBox({
      condition: platformModifierKeyOnly,
    })
  );

  // Map configuration and search results processing
  useEffect(() => {
    map.setTarget('map');
    map.addInteraction(select);
    map.on('loadstart', () => {
      setIsLoading(true);
    });
    map.on('loadend', () => {
      setIsLoading(false);
    });
    map.on('moveend', () => onMapMoveEndCallback());
    map.on('wheel', (e) => onMapWheelScrollCallback(e));
    map.on('pointermove', (e) => onMapPointerMoveCallback(e));
    processData();
  }, [map, selectedFeatures]);

  // Create a new dragBox instance when polygonSource changes
  useEffect(() => {
    setDragBox(
      new DragBox({
        condition: platformModifierKeyOnly,
      })
    );
  }, [polygonSource, selectionToolMode]);

  // Create event listeners for dragBox when it changes
  useEffect(() => {
    // Remove previous DragBox instances before adding the new one
    map.getInteractions().forEach((interaction) => {
      if (interaction?.box_?.element_?.className === 'ol-box ol-dragbox') {
        map.removeInteraction(interaction);
      }
    });
    dragBox.on('boxstart', () => onBoxStartCallback());
    dragBox.on('boxend', () => onBoxEndCallback());
    map.addInteraction(dragBox);
  }, [dragBox]);

  // Update state on filters selection
  useEffect(() => {
    const layers = map.getLayers();
    for (let i = 0; i < filterOptions.length; i++) {
      for (let j = 0; j < layers.getLength(); j++) {
        const mapLayer = map.getLayers().item(j);
        const mapLayerName = mapLayer.get('name');
        if (
          selectedFilterOptions.length !== 0 &&
          mapLayerName === selectedFilterOptions[0].value
        ) {
          // If same filter as before is selected, do nothing
          return;
        }
        if (filterOptions[i].value === mapLayerName) {
          // Remove previously selected filter
          map.removeLayer(mapLayer);
        }
      }
    }
    // If no filter selected, return
    if (selectedFilterOptions.length === 0) {
      return;
    }
    if (selectedFilterOptions[0].value === 'ResRequete') {
      // Update polygonSource object for dragBox selection
      setPolygonSource(map.getLayers().item(getLayerIndex('queryResults')).getSource());
      // Only toggle query results filter if already hidden
      if (!mapLayersVisibility[getLayerIndex('queryResults')]) {
        setLayerDisplay('queryResults', true);
      }
    } else {
      // Update polygonSource object for dragBox selection
      setPolygonSource(mapFilters[selectedFilterOptions[0].value].get('source'));
      // Display newly selected filter
      map.addLayer(mapFilters[selectedFilterOptions[0].value]);
    }
  }, [selectedFilterOptions]);

  // pointData.id set to 0 means that the point is a selected point
  const createPointFeature = (pointData, coordinates, radius, isSelected) => {
    const pointFeature = new Feature({
      geometry: new Circle(coordinates, radius),
    });
    pointFeature.set('id', pointData.id);
    pointFeature.set('nom', pointData.name);
    // If point is a 'selected point', link its id to it.
    if (pointData.linkedPointId) {
      pointFeature.set('linkedPointId', pointData.linkedPointId);
    }
    pointFeature.set('isSelected', isSelected);
    pointFeature.setStyle(pointStyle);
    return pointFeature;
  };

  // On new selection start, unselect features
  const onBoxStartCallback = () => {
    clearSelectedFeatures();
    setIsLoading(true);
  };

  // On new selection end, add all previously selected features
  const onBoxEndCallback = () => {
    let newSelectedPointsIds = selectedPointsIds;
    const boxExtent = dragBox.getGeometry().getExtent();
    // if the extent crosses the antimeridian process each world separately
    const worldExtent = map.getView().getProjection().getExtent();
    const worldWidth = getWidth(worldExtent);
    const startWorld = Math.floor((boxExtent[0] - worldExtent[0]) / worldWidth);
    const endWorld = Math.floor((boxExtent[2] - worldExtent[0]) / worldWidth);
    for (let world = startWorld; world <= endWorld; ++world) {
      const left = Math.max(boxExtent[0] - world * worldWidth, worldExtent[0]);
      const right = Math.min(boxExtent[2] - world * worldWidth, worldExtent[2]);
      const extent = [left, boxExtent[1], right, boxExtent[3]];
      // Retrieve features (points) from source (depending on selected filter).
      // Only unselected points and contained in dragBox
      const boxFeatures = polygonSource
        .getFeaturesInExtent(extent)
        .filter(
          (feature) =>
            !selectedFeatures.getArray().includes(feature) &&
            feature.getGeometry().intersectsExtent(extent)
        );
      const pointsFeatures = map
        .getLayers()
        .item(getLayerIndex('queryResults'))
        .getSource()
        .getFeatures();
      const selectedPointsSource = map
        .getLayers()
        .item(getLayerIndex('selectedPointsSource'))
        .getSource();
      for (let polygon = 0; polygon < boxFeatures.length; polygon++) {
        const polygonGeometry = boxFeatures[polygon].getGeometry();
        // If selection tool is in select mode
        if (!selectionToolMode) {
          for (let point = 0; point < pointsFeatures.length; point++) {
            const pointGeom = pointsFeatures[point].getGeometry();
            const coords = pointGeom.getCenter();
            if (polygonGeometry.intersectsCoordinate(coords)) {
              const pointName = pointsFeatures[point].get('nom');
              const pointId = pointsFeatures[point].get('id');
              if (!getSelectedPointsNames(selectedPointsSource).includes(pointName)) {
                // Add new selected features
                const pointFeature = createPointFeature(
                  {
                    id: 0,
                    name: pointName,
                    linkedPointId: pointId,
                  },
                  coords,
                  5000,
                  true
                );
                selectedPointsSource.addFeature(pointFeature);
                // Add point id to selected points Ids
                newSelectedPointsIds.push(pointId);
              }
            }
          }
          // If selection tool is in unselect mode
        } else {
          const selectedPointsFeatures = selectedPointsSource.getFeatures();
          if (selectedPointsFeatures.length > 0) {
            for (let i = 0; i < selectedPointsFeatures.length; i++) {
              const point = selectedPointsFeatures[i];
              const pointGeom = point.getGeometry();
              const coords = pointGeom.getCenter();
              if (polygonGeometry.intersectsCoordinate(coords)) {
                const pointId = point.get('linkedPointId');
                unselectPoint(pointId, selectedPointsSource);
              }
            }
          }
        }
      }
      if (!selectionToolMode) {
        let tmpSelectedFeatures = selectedFeatures;
        setSelectedFeatures(null);
        tmpSelectedFeatures.extend(boxFeatures);
        setSelectedFeatures(tmpSelectedFeatures);
      } else {
        clearSelectedFeatures();
      }
    }
    // Update selected features list
    setSelectedPointsIds(newSelectedPointsIds);
    setIsLoading(false);
  };

  const processData = () => {
    if (!searchResults) {
      return;
    }
    const resultPointsSource = map
      .getLayers()
      .item(getLayerIndex('queryResults'))
      .getSource();
    // Create points from search results
    searchResults.forEach((result) => {
      const geoPoint = result.experimental_site.geo_point;
      if (geoPoint && geoPoint.longitude && geoPoint.latitude) {
        const pointFeature = createPointFeature(
          {
            id: result.id,
            name: result?.resource?.identifier,
          },
          proj.fromLonLat([geoPoint.longitude, geoPoint.latitude]),
          3500,
          false
        );
        resultPointsSource.addFeature(pointFeature);
      }
    });
    const selectedPointsSource = mapFilters.selectedPointsLayer.getSource();
    // Create selected points from id list
    selectedPointsIds.forEach((id) => {
      const point = searchResults.find((result) => result.id === id);
      if (point) {
        const geoPoint = point.experimental_site.geo_point;
        const pointName = point.resource.identifier;
        if (geoPoint && geoPoint.longitude && geoPoint.latitude) {
          if (!getSelectedPointsNames(selectedPointsSource).includes(pointName)) {
            const pointFeature = createPointFeature(
              {
                id: 0,
                name: pointName,
                linkedPointId: id,
              },
              proj.fromLonLat([geoPoint.longitude, geoPoint.latitude]),
              5000,
              true
            );
            selectedPointsSource.addFeature(pointFeature);
          }
        }
      }
    });
  };

  const clearSelectedFeatures = () => {
    let tmpSelectedFeatures = selectedFeatures;
    if (tmpSelectedFeatures) {
      tmpSelectedFeatures.clear();
    }
    setSelectedFeatures(tmpSelectedFeatures);
  };

  const onMapMoveEndCallback = () => {
    setCenter(map.getView().getCenter());
    setZoom(map.getView().getZoom());
  };

  // Display helper text inside map on mouse wheel scroll
  const onMapWheelScrollCallback = (e) => {
    // Return if user is zooming correctly
    if (e.originalEvent.ctrlKey) {
      return;
    }
    if (!mapLayersVisibility['zoomHelperText']) {
      setLayerDisplay('zoomHelperText', true);
      timerRef.current = setTimeout(() => {
        setLayerDisplay('zoomHelperText', false);
      }, 1500);
    }
  };

  // Clear timer ref on unmount
  useEffect(() => {
    return () => clearTimeout(timerRef.current);
  }, []);

  // Display pointer cursor on feature hover
  const onMapPointerMoveCallback = (e) => {
    const pixel = map.getEventPixel(e.originalEvent);
    const hit = map.hasFeatureAtPixel(pixel);
    map.getViewport().style.cursor = hit ? 'pointer' : '';
  };

  const toggleSelectionToolMode = () => {
    setSelectionToolMode((prev) => !prev);
  };

  const onFilterSelectChange = (newSelectedOptions) => {
    setSelectedFilterOptions(newSelectedOptions);
  };

  const getSelectedPointsNames = (source) => {
    let featureNames = [];
    source.getFeatures().forEach((feature) => {
      const name = feature.get('nom');
      if (name) {
        featureNames.push(name);
      }
    });
    return featureNames;
  };

  const setLayerDisplay = (name, isShown) => {
    const layerIndex = getLayerIndex(name);
    let updatedLayers = mapLayers;
    setMapLayersVisibility(updateArrayElement(mapLayersVisibility, layerIndex, isShown));
    updatedLayers[layerIndex].setVisible(isShown);
    setMapLayers(updatedLayers);
  };

  const getSelectedPoints = () => {
    const selectedPointsLayer = map
      .getLayers()
      .item(getLayerIndex('selectedPointsSource'));
    const selectedPointsSource = selectedPointsLayer.getSource();
    if (!selectedPointsLayer || !selectedPointsSource) {
      return [];
    }
    const features = selectedPointsSource.getFeatures();
    if (!features) {
      return [];
    }
    const points = [];
    features.forEach((feature) => {
      points.push(feature.getProperties());
    });
    return points;
  };

  const getPoints = () => {
    const pointsSource = map.getLayers().item(getLayerIndex('queryResults')).getSource();
    const features = pointsSource.getFeatures();
    if (!features) {
      return [];
    }
    const points = [];
    features.forEach((feature) => {
      points.push(feature.getProperties());
    });
    return points;
  };

  // Unselect a single point. Remove it from map display and from selected points ids list.
  const unselectPoint = (id, source) => {
    let newSelectedPointsIds = selectedPointsIds;
    if (!source) {
      source = map.getLayers().item(getLayerIndex('selectedPointsSource')).getSource();
    }
    const selectedPointsFeatures = source.getFeatures();
    if (!selectedPointsFeatures || selectedPointsFeatures.length === 0) {
      return;
    }
    const featureToRemove = selectedPointsFeatures.find(
      (feature) => feature.get('linkedPointId') === id
    );
    if (!featureToRemove) {
      return;
    }
    source.removeFeature(featureToRemove);
    const indexOfPointToRemove = newSelectedPointsIds.indexOf(id);
    if (indexOfPointToRemove >= 0) {
      newSelectedPointsIds = newSelectedPointsIds.toSpliced(indexOfPointToRemove, 1);
    }
    // Remove previously selected features
    setSelectedPointsIds(newSelectedPointsIds);
  };

  // Display selected points names
  const SelectedPointsList = () => {
    const SelectedPointItem = ({ id, name }) => {
      const onUncheckButtonClick = () => {
        unselectPoint(id);
      };

      const onOpenFlyoutClick = () => {
        setResourceFlyoutDataFromId(id);
        setIsResourceFlyoutOpen(true);
      };

      return (
        <EuiPanel paddingSize="s" hasShadow={false} hasBorder={true}>
          <EuiFlexGroup alignItems={'center'}>
            <EuiFlexGroup>
              <p>{name}</p>
            </EuiFlexGroup>
            <EuiToolTip
              position="top"
              content={<p>{t('maps:selectedPointsList.actions.openResourceFlyout')}</p>}
            >
              <EuiButtonIcon
                iconType="eye"
                aria-label={t('maps:selectedPointsList.actions.openResourceFlyout')}
                onClick={() => onOpenFlyoutClick()}
                color={'primary'}
                display={'base'}
                size={'s'}
              />
            </EuiToolTip>
            <EuiToolTip
              position="top"
              content={<p>{t('maps:selectedPointsList.actions.unselectResource')}</p>}
            >
              <EuiButtonIcon
                iconType="cross"
                aria-label={t('maps:selectedPointsList.actions.unselectResource')}
                onClick={() => onUncheckButtonClick()}
                color={'danger'}
                size={'s'}
              />
            </EuiToolTip>
            <EuiSpacer />
          </EuiFlexGroup>
        </EuiPanel>
      );
    };

    const buildSelectedPointList = () => {
      return selectedPoints.map((point, index) => {
        return (
          <SelectedPointItem key={index} name={point.nom} id={point.linkedPointId} />
        );
      });
    };

    let selectedPointsList;
    let totalPoints = getPoints();
    let selectedPoints = getSelectedPoints();
    if (selectedPoints.length === 0) {
      selectedPointsList = <p>{t('maps:selectedPointsList.empty')}</p>;
    } else {
      selectedPointsList = buildSelectedPointList();
    }

    return (
      <EuiPanel hasShadow={false} hasBorder={true}>
        <EuiFlexGroup direction={'column'}>
          <EuiTitle size="s">
            <p>
              {t('maps:selectedPointsList.title')} ({selectedPoints.length} /{' '}
              {totalPoints.length})
            </p>
          </EuiTitle>
          <EuiFlexGroup direction={'column'} style={styles.selectedPointsList}>
            {selectedPointsList}
          </EuiFlexGroup>
        </EuiFlexGroup>
      </EuiPanel>
    );
  };

  const selectionToolOptions = [
    {
      id: 'selectionToolButton__0',
      label: t('maps:layersTable.selectionTool.select'),
    },
    {
      id: 'selectionToolButton__1',
      label: t('maps:layersTable.selectionTool.unselect'),
    },
  ];

  const unselectPoints = () => {
    // Empty point selection
    clearSelectedFeatures();
    // Remove displayed points from layer's source
    map.getLayers().item(getLayerIndex('selectedPointsSource')).getSource().clear();
    setSelectedPointsIds([]);
  };

  const MapTools = () => {
    return (
      <EuiPanel paddingSize="l" hasShadow={false} hasBorder={true}>
        <EuiFlexGroup>
          <table style={styles.layersTable}>
            <thead>
              <tr>
                <th>{t('maps:layersTableHeaders.cartography')}</th>
                <th>{t('maps:layersTableHeaders.filters')}</th>
                <th>{t('maps:layersTableHeaders.tools')}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style={styles.layersTableCells}>
                  <EuiCheckbox
                    id={htmlIdGenerator()()}
                    label={t('maps:layersTable.openStreetMap')}
                    checked={mapLayersVisibility[getLayerIndex('osm-layer')]}
                    onChange={(e) => setLayerDisplay('osm-layer', e.target.checked)}
                  />
                  <EuiCheckbox
                    id={htmlIdGenerator()()}
                    label={t('maps:layersTable.bingAerial')}
                    checked={mapLayersVisibility[getLayerIndex('Bing Aerial')]}
                    onChange={(e) => setLayerDisplay('Bing Aerial', e.target.checked)}
                  />
                  <EuiCheckbox
                    id={htmlIdGenerator()()}
                    label={t('maps:layersTable.IGN')}
                    checked={mapLayersVisibility[getLayerIndex('IGN')]}
                    onChange={(e) => setLayerDisplay('IGN', e.target.checked)}
                  />
                </td>
                <td style={styles.layersTableCells}>
                  <EuiCheckbox
                    id={htmlIdGenerator()()}
                    label={t('maps:layersTable.queryResults')}
                    checked={mapLayersVisibility[getLayerIndex('queryResults')]}
                    onChange={(e) => setLayerDisplay('queryResults', e.target.checked)}
                  />
                  <br />
                  <EuiComboBox
                    aria-label={t('maps:layersTable.selectFilterOption')}
                    placeholder={t('maps:layersTable.selectFilterOption')}
                    singleSelection={{ asPlainText: true }}
                    options={filterOptions}
                    selectedOptions={selectedFilterOptions}
                    onChange={onFilterSelectChange}
                    styles={styles.filtersSelect}
                  />
                </td>
                <td style={styles.layersTableCells}>
                  <EuiTitle size="xxs">
                    <h6>{t('maps:layersTable.selectionTool.title')}</h6>
                  </EuiTitle>
                  <EuiButtonGroup
                    legend={t('maps:layersTable.selectionTool.title')}
                    options={selectionToolOptions}
                    onChange={toggleSelectionToolMode}
                    idSelected={
                      selectionToolMode
                        ? 'selectionToolButton__1'
                        : 'selectionToolButton__0'
                    }
                    color={'primary'}
                    isFullWidth
                  />
                  <EuiSpacer size="s" />
                  <EuiButton
                    onClick={() => unselectPoints()}
                    style={styles.unselectAllButton}
                    color={'accent'}
                    disabled={selectedPointsIds.length === 0}
                  >
                    {t('maps:layersTable.selectionTool.unselectAll')}
                  </EuiButton>
                </td>
              </tr>
            </tbody>
          </table>
        </EuiFlexGroup>
      </EuiPanel>
    );
  };

  return (
    <EuiFlexGroup>
      <EuiFlexGroup direction={'column'} style={styles.container}>
        <EuiFlexItem>
          <div id="map" style={styles.mapContainer}></div>
          {isLoading && <EuiProgress size="l" color="accent" />}
        </EuiFlexItem>
        <MapTools />
      </EuiFlexGroup>
      <SelectedPointsList />
    </EuiFlexGroup>
  );
};

export default SearchMap;
