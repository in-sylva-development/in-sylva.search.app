import React, { useState, useEffect } from 'react';
import {
  EuiTitle,
  EuiButton,
  EuiFormRow,
  EuiComboBox,
  EuiBasicTable,
  EuiFlexItem,
  EuiPanel,
  EuiFlexGroup,
  EuiSpacer,
} from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import ToastMessage from '../../components/ToastMessage/ToastMessage';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import { useUserInfo } from '../../contexts/TokenContext';

const GroupSettings = ({ userGroups }) => {
  const getUserInfo = useUserInfo();
  const client = useGatekeeper();
  const { t } = useTranslation(['profile', 'common', 'validation']);
  const [groups, setGroups] = useState([]);
  const [selectedUserGroups, setSelectedUserGroups] = useState([]);
  const [valueError, setValueError] = useState(undefined);

  useEffect(() => {
    const getUserGroups = async () => {
      if (!client) {
        return;
      }
      const groupResults = await client.getGroups();
      const mappedGroups = groupResults.map((group) => {
        return {
          id: group.id,
          label: group.name,
          description: group.description,
          member: userGroups.some((userGroup) => userGroup.id === group.id) ? 'X' : '',
        };
      });
      setGroups(mappedGroups);
    };
    getUserGroups();
  }, []);

  const groupColumns = [
    { field: 'label', name: t('profile:groups.groupName'), width: '30%' },
    { field: 'description', name: t('profile:groups.groupDescription') },
    {
      field: 'member',
      name: t('profile:groups.groupMember'),
      width: '10%',
      align: 'center',
    },
  ];

  const onValueSearchChange = (value, hasMatchingOptions) => {
    if (value.length !== 0 && !hasMatchingOptions) {
      setValueError(t('profile:groupRequests.invalidOption'));
    }
  };

  const onSendGroupRequest = async () => {
    const userInfo = await getUserInfo();
    if (!selectedUserGroups || selectedUserGroups.length === 0) {
      return;
    }
    const message = `The user ${userInfo.email} has made a request to be part of these groups : ${selectedUserGroups
      .map((group) => group.label)
      .join(', ')}.`;
    const result = await client.createUserRequest(message, userInfo.sub);
    if (result.error) {
      toast.error(<ToastMessage title={t('validation:error')} message={result.error} />);
    } else {
      toast.success(t('validation:requestSent'));
    }
  };

  return (
    userGroups &&
    userGroups.length > 0 && (
      <EuiFlexGroup>
        <EuiFlexItem>
          <EuiPanel paddingSize="l" hasShadow={false} hasBorder={true}>
            <EuiTitle size="s">
              <h3>{t('profile:groups.groupsList')}</h3>
            </EuiTitle>
            <EuiSpacer size={'l'} />
            <EuiBasicTable items={groups} columns={groupColumns} />
          </EuiPanel>
        </EuiFlexItem>

        <EuiFlexItem>
          <EuiPanel paddingSize="l" hasShadow={false} hasBorder={true}>
            <EuiTitle size="s">
              <h3>{t('profile:groupRequests.requestGroupAssignment')}</h3>
            </EuiTitle>
            <EuiSpacer size={'l'} />
            <EuiFormRow error={valueError} isInvalid={valueError !== undefined}>
              <EuiComboBox
                placeholder={t('profile:groupRequests.selectGroup')}
                options={groups.filter((group) => group.member != 'X')}
                selectedOptions={selectedUserGroups}
                onChange={(selectedOptions) => {
                  setValueError(undefined);
                  setSelectedUserGroups(selectedOptions);
                }}
                onSearchChange={onValueSearchChange}
              />
            </EuiFormRow>
            <EuiSpacer size={'l'} />

            <EuiFlexItem>
              <div>
                <EuiButton
                  disabled={selectedUserGroups.length === 0}
                  onClick={() => {
                    onSendGroupRequest();
                  }}
                  fill
                >
                  {t('common:validationActions.send')}
                </EuiButton>
              </div>
            </EuiFlexItem>
          </EuiPanel>
        </EuiFlexItem>
      </EuiFlexGroup>
    )
  );
};

export default GroupSettings;
