import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { EuiPanel, EuiTabbedContent, EuiFlexGroup, EuiCallOut } from '@elastic/eui';
import UserFieldsDisplaySettings from './UserFieldsDisplaySettings';
import GroupSettings from './GroupSettings';
import RoleSettings from './RoleSettings';
import MyProfile from './MyProfile';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import { useUserInfo } from '../../contexts/TokenContext';

const Profile = () => {
  const getUserInfo = useUserInfo();
  const client = useGatekeeper();
  const { t } = useTranslation(['profile', 'common', 'validation']);
  const [selectedTabNumber, setSelectedTabNumber] = useState(0);
  const [userGroups, setUserGroups] = useState([]);
  const [userRole, setUserRole] = useState('');
  const [userRequests, setUserRequests] = useState([]);
  const [fieldsDisplaySettings, setFieldsDisplaySettings] = useState(null);
  const [publicFields, setPublicFields] = useState([]);

  const fetchUser = async () => {
    const { sub } = await getUserInfo();
    const user = await client.findUserBySub(sub);
    if (!user.id) {
      return;
    }
    const { groups, roles, requests, display_fields } = user;
    setUserGroups(
      groups.map((group) => {
        return {
          id: group.group.id,
          name: group.group.name,
        };
      })
    );
    setUserRole(roles[0]?.role?.name);
    setUserRequests(requests);
    setFieldsDisplaySettings(display_fields.map((field) => field.std_field_id));
  };

  const fetchFields = async () => {
    const fields = await client.getPublicFields();
    setPublicFields(fields);
  };

  useEffect(() => {
    if (client && selectedTabNumber === 0) {
      fetchUser();
      fetchFields();
    }
  }, [selectedTabNumber]);

  const Tab = ({ children, description }) => {
    const [showCallOut, setShowCallOut] = useState(true);

    const onDismiss = () => {
      setShowCallOut(false);
    };

    return (
      <EuiPanel
        style={{
          minHeight: '85vh',
        }}
        paddingSize="l"
      >
        <EuiFlexGroup direction={'column'}>
          {showCallOut && description && (
            <EuiCallOut
              onDismiss={onDismiss}
              size="s"
              title={description}
              iconType="iInCircle"
            />
          )}
          {children}
        </EuiFlexGroup>
      </EuiPanel>
    );
  };

  const tabsContent = [
    {
      id: 'tab1',
      name: t('profile:tabs.profile'),
      content: (
        <Tab>
          <MyProfile
            setSelectedTabNumber={setSelectedTabNumber}
            userGroups={userGroups}
            userRole={userRole}
            userRequests={userRequests}
            fieldsDisplaySettingsIds={fieldsDisplaySettings}
            publicFields={publicFields}
            getUserRequests={() => fetchUser()}
          />
        </Tab>
      ),
    },
    {
      id: 'tab2',
      name: t('profile:tabs.groups'),
      content: (
        <Tab description={t('profile:myProfile.groupPanel.description')}>
          <GroupSettings userGroups={userGroups} />
        </Tab>
      ),
    },
    {
      id: 'tab3',
      name: t('profile:tabs.role'),
      content: (
        <Tab description={t('profile:myProfile.rolePanel.description')}>
          <RoleSettings userRole={userRole} />
        </Tab>
      ),
    },
    {
      id: 'tab4',
      name: t('profile:tabs.fieldsDisplaySettings'),
      content: (
        <Tab description={t('profile:myProfile.fieldsDisplaySettingsPanel.description')}>
          <UserFieldsDisplaySettings
            userSettings={fieldsDisplaySettings}
            setUserSettings={setFieldsDisplaySettings}
            publicFields={publicFields}
          />
        </Tab>
      ),
    },
  ];

  return (
    <EuiTabbedContent
      tabs={tabsContent}
      selectedTab={tabsContent[selectedTabNumber]}
      onTabClick={(tab) => {
        setSelectedTabNumber(tabsContent.indexOf(tab));
      }}
    />
  );
};

export default Profile;
