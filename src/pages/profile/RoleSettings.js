import React, { useState, useEffect } from 'react';
import { useAuth } from 'oidc-react';
import {
  EuiTitle,
  EuiSelect,
  EuiButton,
  EuiFormRow,
  EuiFlexItem,
  EuiPanel,
  EuiSpacer,
  EuiFlexGroup,
} from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import { toast } from 'react-toastify';
import ToastMessage from '../../components/ToastMessage/ToastMessage';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import { useUserInfo } from '../../contexts/TokenContext';

const RoleSettings = ({ userRole }) => {
  const client = useGatekeeper();
  const getUserInfo = useUserInfo();
  const { t } = useTranslation(['profile', 'common', 'validation']);
  const [roles, setRoles] = useState([]);
  const [selectedRole, setSelectedRole] = useState(undefined);

  useEffect(() => {
    const fetchRoles = async () => {
      const roles = await client.getRoles();
      const filteredRoles = roles
        .filter((role) => role.name !== userRole)
        .map((role) => ({ value: role.name, text: role.name }));
      setRoles(filteredRoles);
    };
    fetchRoles();
  }, [client]);

  const onSendRoleRequest = async () => {
    const { email, sub } = await getUserInfo();
    if (selectedRole) {
      const message = `The user ${email} has made a request to get the role : ${selectedRole}.`;
      const result = await client.createUserRequest(message, sub);
      if (result.error) {
        toast.error(
          <ToastMessage title={t('validation:error')} message={result.error} />
        );
      } else {
        toast.success(t('validation:requestSent'));
      }
    }
  };

  return (
    <EuiFlexGroup>
      <EuiFlexItem grow={false}>
        <EuiPanel paddingSize="l" hasShadow={false} hasBorder={true}>
          <EuiTitle size="s">
            <p>{t('profile:roleRequests.requestRoleAssignment')}</p>
          </EuiTitle>
          <EuiSpacer size={'l'} />
          {userRole && (
            <p style={styles.currentRoleOrGroupText}>
              {`${t('profile:roleRequests.currentRole')} ${userRole}`}
            </p>
          )}
          <EuiSpacer size={'l'} />
          <EuiFormRow>
            <EuiSelect
              hasNoInitialSelection
              options={roles}
              value={selectedRole}
              onChange={(e) => {
                setSelectedRole(e.target.value);
              }}
            />
          </EuiFormRow>
          <EuiSpacer size={'l'} />
          <EuiButton onClick={() => onSendRoleRequest()} fill disabled={!selectedRole}>
            {t('common:validationActions.send')}
          </EuiButton>
        </EuiPanel>
      </EuiFlexItem>
    </EuiFlexGroup>
  );
};

export default RoleSettings;
