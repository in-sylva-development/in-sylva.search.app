import React, { useEffect, useState } from 'react';
import {
  EuiSpacer,
  EuiSelectable,
  EuiButton,
  EuiFlexItem,
  EuiFlexGroup,
  EuiPanel,
  EuiTitle,
} from '@elastic/eui';
import { Trans, useTranslation } from 'react-i18next';
import { buildFieldName } from '../../Utils';
import BulletPointList from '../../components/BulletPointList/BulletPointList';
import { toast } from 'react-toastify';
import ToastMessage from '../../components/ToastMessage/ToastMessage';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import { useUserInfo } from '../../contexts/TokenContext';

/*
  User fields display settings are used to choose which fields are displayed in results table after a search.
  If the user has no settings, the default are used. Default settings are the same for all users, chosen by admin at standard setup.
 */
const UserFieldsDisplaySettings = ({ userSettings, setUserSettings, publicFields }) => {
  const client = useGatekeeper();
  const getUserInfo = useUserInfo();
  const { t } = useTranslation(['profile', 'common', 'validation']);
  const [settingsOptions, setSettingsOptions] = useState([]);
  const [selectedOptionsIds, setSelectedOptionsIds] = useState([]);
  const [selectedOptions, setSelectedOptions] = useState([]);

  useEffect(() => {
    if (userSettings) {
      setSelectedOptionsIds(userSettings);
    } else {
      // TODO fetch global default settings so they can be selected by default.
      setSelectedOptionsIds([1]);
    }
    if (publicFields) {
      setSettingsOptions(buildSettingsOptions());
    }
  }, []);

  // On each settingsOptions change, update selected ids and names accordingly
  useEffect(() => {
    const newSelectedOptionsIds = [];
    const newSelectedOptions = [];
    settingsOptions.forEach((option) => {
      if (option.checked === 'on') {
        newSelectedOptionsIds.push(option.id);
        newSelectedOptions.push(option.label);
      }
    });
    setSelectedOptionsIds(newSelectedOptionsIds);
    setSelectedOptions(newSelectedOptions);
  }, [settingsOptions]);

  const isOptionChecked = (fieldId) => {
    if (!userSettings) {
      return undefined;
    }
    return userSettings.includes(fieldId) ? 'on' : undefined;
  };

  const buildSettingsOptions = () => {
    if (!publicFields) {
      return [];
    }
    const newSettingsOptions = [];
    // TODO disabled attribute only to fields where cardinality is [1-n]
    publicFields.forEach((field) => {
      if (field && field.field_name) {
        newSettingsOptions.push({
          id: field.id,
          label: buildFieldName(field.field_name),
          checked: isOptionChecked(field.id),
          toolTipContent: field.definition_and_comment,
          toolTipProps: { position: 'bottom' },
        });
      }
    });
    return newSettingsOptions;
  };

  // On save settings button click, create or update user settings in database
  const onSaveSettings = async () => {
    if (!selectedOptionsIds || selectedOptionsIds.length === 0) {
      return;
    }
    let result;
    const { sub } = await getUserInfo();
    const response = await client.setUserFieldsDisplaySettings(sub, selectedOptionsIds);
    if (response && response.length == 0) {
      toast.error(<ToastMessage title={t('validation:error')} message={result.error} />);
    } else {
      setUserSettings(selectedOptionsIds);
      toast.success(t('profile:fieldsDisplaySettings.updatedSettingsSuccess'));
    }
  };

  // Reset selection to currently saved user settings.
  const onSelectionReset = () => {
    if (!settingsOptions || !userSettings) {
      toast.error(t('profile:fieldsDisplaySettings.selectionResetFailure'));
      return;
    }
    const newSettingsOptions = [];
    settingsOptions.forEach((option) => {
      option.checked = isOptionChecked(option.id);
      newSettingsOptions.push(option);
    });
    setSettingsOptions(newSettingsOptions);
    toast(t('profile:fieldsDisplaySettings.selectionReset'));
  };

  // Reset user settings to system default.
  // With current logic, no user settings means it should use default.
  // So in this case 'reset' means delete current user settings.
  const onSettingsReset = async () => {
    if (!settingsOptions) {
      toast.error(t('profile:fieldsDisplaySettings.selectionResetFailure'));
      return;
    }
    const newSettingsOptions = [];
    settingsOptions.forEach((option) => {
      option.checked = false;
      newSettingsOptions.push(option);
    });
    setSettingsOptions(newSettingsOptions);
  };

  const SelectableSettingsPanel = () => {
    return (
      <EuiFlexItem>
        <EuiPanel paddingSize="l" hasShadow={false} hasBorder={true}>
          <EuiSelectable
            aria-label={t('profile:fieldsDisplaySettings.selectedOptionsLabel')}
            options={settingsOptions}
            onChange={(newOptions) => setSettingsOptions(newOptions)}
            searchable
            listProps={{ bordered: true }}
            style={{ minHeight: '65vh' }}
            height={'full'}
          >
            {(list, search) => (
              <>
                {search}
                <EuiSpacer size={'xs'} />
                {list}
              </>
            )}
          </EuiSelectable>
        </EuiPanel>
      </EuiFlexItem>
    );
  };

  const SelectedSettingsPanel = () => {
    const SelectedSettingsList = () => {
      return (
        <BulletPointList>
          {selectedOptions.map((option, index) => {
            return <li key={index}>{option}</li>;
          })}
        </BulletPointList>
      );
    };

    return (
      <EuiFlexItem>
        <EuiPanel grow={false} paddingSize="l" hasShadow={false} hasBorder={true}>
          <EuiTitle size={'s'}>
            <p>
              <Trans
                i18nKey={'profile:fieldsDisplaySettings.selectedOptionsNumber'}
                count={selectedOptionsIds.length}
              />
            </p>
          </EuiTitle>
          <EuiSpacer size={'l'} />
          <SelectedSettingsList />
          <EuiSpacer size={'l'} />
          <EuiFlexGroup justifyContent={'spaceBetween'}>
            <EuiButton
              onClick={() => onSaveSettings()}
              fill
              disabled={selectedOptionsIds.length === 0}
            >
              {t('common:validationActions.save')}
            </EuiButton>
            <div>
              <EuiFlexGroup>
                <EuiButton onClick={() => onSelectionReset()}>
                  {t('profile:fieldsDisplaySettings.resetSelection')}
                </EuiButton>
                <EuiButton color={'danger'} onClick={() => onSettingsReset()}>
                  {t('profile:fieldsDisplaySettings.deleteSettings')}
                </EuiButton>
              </EuiFlexGroup>
            </div>
          </EuiFlexGroup>
        </EuiPanel>
      </EuiFlexItem>
    );
  };

  return (
    <EuiFlexGroup>
      <SelectableSettingsPanel />
      <SelectedSettingsPanel />
    </EuiFlexGroup>
  );
};

export default UserFieldsDisplaySettings;
