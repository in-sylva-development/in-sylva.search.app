import { useTranslation } from 'react-i18next';
import React from 'react';
import {
  EuiBasicTable,
  EuiButtonEmpty,
  EuiFlexGroup,
  EuiFlexItem,
  EuiPanel,
  EuiSpacer,
  EuiTitle,
  EuiIconTip,
} from '@elastic/eui';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import { buildFieldName } from '../../Utils';
import BulletPointList from '../../components/BulletPointList/BulletPointList';
import { toast } from 'react-toastify';
import ToastMessage from '../../components/ToastMessage/ToastMessage';

const MyProfile = ({
  setSelectedTabNumber,
  userGroups,
  userRole,
  userRequests,
  fieldsDisplaySettingsIds,
  publicFields,
  getUserRequests,
}) => {
  const client = useGatekeeper();
  const { t } = useTranslation(['profile']);

  const MyProfileCustomPanel = ({
    title,
    description,
    linkedTabNumber,
    linkedTabButton,
    children,
  }) => {
    return (
      <EuiFlexItem>
        <EuiPanel paddingSize="l" hasShadow={false} hasBorder={true}>
          <EuiFlexGroup justifyContent={'spaceBetween'} alignItems={'center'}>
            <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
              <EuiTitle size="xs">
                <p>{title}</p>
              </EuiTitle>
              {description && (
                <div style={{ marginLeft: '10px' }}>
                  <EuiIconTip
                    size="l"
                    color="primary"
                    position="right"
                    content={description}
                  />
                </div>
              )}
            </div>
            {linkedTabNumber && (
              <EuiButtonEmpty
                size="s"
                onClick={() => setSelectedTabNumber(linkedTabNumber)}
              >
                {linkedTabButton}
              </EuiButtonEmpty>
            )}
          </EuiFlexGroup>
          <EuiSpacer size={'m'} />
          <EuiFlexGroup direction={'column'}>{children}</EuiFlexGroup>
        </EuiPanel>
      </EuiFlexItem>
    );
  };

  const onDeleteRequest = async (request) => {
    const { success, message } = await client.deleteUserRequest(request.id);
    if (!success) {
      toast.error(<ToastMessage title={t('validation:error')} message={message} />);
    } else {
      toast.success(t('profile:requestsList.requestCanceled'));
      // Refresh requests table
      getUserRequests();
    }
  };

  const requestActions = [
    {
      name: t('common:validationActions.cancel'),
      description: t('profile:requestsList.cancelRequest'),
      icon: 'trash',
      type: 'icon',
      onClick: onDeleteRequest,
    },
  ];

  const requestsColumns = [
    {
      field: 'request_message',
      name: t('profile:requestsList.requestsMessage'),
      width: '85%',
    },
    {
      field: 'is_processed',
      name: t('profile:requestsList.processed'),
      render: (isProcessed) => {
        return (
          <p>
            {isProcessed
              ? t('common:validationActions.yes')
              : t('common:validationActions.no')}
          </p>
        );
      },
    },
    { name: t('common:validationActions.cancel'), actions: requestActions },
  ];

  const GroupList = () => {
    if (!userGroups || userGroups.length === 0) {
      return <p>{t('profile:groupRequests.noGroup')}</p>;
    }
    const listItems = userGroups.map((group, index) => <li key={index}>{group.name}</li>);
    return <BulletPointList>{listItems}</BulletPointList>;
  };

  const FieldsDisplaySettings = () => {
    if (!fieldsDisplaySettingsIds || fieldsDisplaySettingsIds.length === 0) {
      return <p>{t('profile:fieldsDisplaySettings.noSettings')}</p>;
    }
    const fieldsDisplaySettings = [];
    publicFields.forEach((field) => {
      if (fieldsDisplaySettingsIds.includes(field.id)) {
        fieldsDisplaySettings.push(buildFieldName(field.field_name));
      }
    });
    const listItems = fieldsDisplaySettings.map((fieldName, index) => (
      <li key={index}>{fieldName}</li>
    ));
    // Have to style list manually because of display flex container
    return <BulletPointList>{listItems}</BulletPointList>;
  };

  return (
    <>
      <EuiTitle>
        <h3>{sessionStorage.getItem('username')}</h3>
      </EuiTitle>
      <EuiFlexGroup>
        <MyProfileCustomPanel
          title={t('profile:myProfile.groupPanel.title')}
          description={t('profile:myProfile.groupPanel.description')}
          linkedTabButton={t('profile:myProfile.groupPanel.edit')}
          linkedTabNumber={1}
        >
          <GroupList />
        </MyProfileCustomPanel>
        <MyProfileCustomPanel
          title={t('profile:myProfile.rolePanel.title')}
          description={t('profile:myProfile.rolePanel.description')}
          linkedTabButton={t('profile:myProfile.rolePanel.edit')}
          linkedTabNumber={2}
        >
          <p>{userRole}</p>
        </MyProfileCustomPanel>
      </EuiFlexGroup>

      <MyProfileCustomPanel
        title={t('profile:myProfile.requestsPanel.title')}
        description={t('profile:myProfile.requestsPanel.description')}
      >
        {userRequests.length !== 0 ? (
          <EuiBasicTable items={userRequests} columns={requestsColumns} />
        ) : (
          <EuiFlexGroup justifyContent={'center'}>
            <p>{t('profile:requestsList.noCurrentRequest')}</p>
          </EuiFlexGroup>
        )}
      </MyProfileCustomPanel>
      <EuiFlexGroup>
        <MyProfileCustomPanel
          title={t('profile:myProfile.fieldsDisplaySettingsPanel.title')}
          description={t('profile:myProfile.fieldsDisplaySettingsPanel.description')}
          linkedTabButton={t('profile:myProfile.fieldsDisplaySettingsPanel.edit')}
          linkedTabNumber={3}
        >
          <FieldsDisplaySettings />
        </MyProfileCustomPanel>
        <MyProfileCustomPanel
          title={t('profile:myProfile.fieldsDownloadSettingsPanel.title')}
          description={t('profile:myProfile.fieldsDownloadSettingsPanel.description')}
          linkedTabButton={t('profile:myProfile.fieldsDownloadSettingsPanel.edit')}
          linkedTabNumber={3}
        >
          <p>Fonctionnalité à venir prochainement.</p>
        </MyProfileCustomPanel>
      </EuiFlexGroup>
    </>
  );
};

export default MyProfile;
