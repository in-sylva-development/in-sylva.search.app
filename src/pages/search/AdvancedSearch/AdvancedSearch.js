import {
  EuiButton,
  EuiButtonEmpty,
  EuiButtonIcon,
  EuiComboBox,
  EuiFieldText,
  EuiFlexGroup,
  EuiFlexItem,
  EuiForm,
  EuiFormRow,
  EuiHealth,
  EuiIcon,
  EuiModal,
  EuiModalBody,
  EuiModalFooter,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiOverlayMask,
  EuiPanel,
  EuiPopover,
  EuiPopoverFooter,
  EuiPopoverTitle,
  EuiRadioGroup,
  EuiSelect,
  EuiSpacer,
  EuiSwitch,
  EuiTextArea,
  EuiTextColor,
  EuiTitle,
  EuiDatePicker,
} from '@elastic/eui';
import React, { Fragment, useEffect, useState } from 'react';
import {
  changeNameToLabel,
  getFieldsBySection,
  getSections,
  removeArrayElement,
  SearchField,
  updateArrayElement,
  updateSearchFieldValues,
} from '../../../Utils';
import { DateOptions, NumericOptions, Operators } from '../Data';
import { useTranslation } from 'react-i18next';
import styles from './styles.js';
import moment from 'moment';
import SearchModeSwitcher from '../SearchModeSwitcher';
import { toast } from 'react-toastify';
import ToastMessage from '../../../components/ToastMessage/ToastMessage';
import { useGatekeeper } from '../../../contexts/GatekeeperContext';
import { useAuth } from 'oidc-react';
import { buildDslQuery } from '../../../Utils';

const updateSources = (
  searchFields,
  sources,
  setSelectedSources,
  setAvailableSources
) => {
  let updatedSources = [];
  let availableSources = [];
  let noPrivateField = true;
  // Search for policy fields to filter sources
  searchFields.forEach((field) => {
    if (field.isValidated) {
      // If sources haven't already been filtered
      if (noPrivateField && !updatedSources.length) {
        availableSources = sources;
      } else {
        availableSources = updatedSources;
      }
      updatedSources = [];
      field.sources?.forEach((sourceId) => {
        noPrivateField = false;
        const source = availableSources.find((src) => src.id === sourceId);
        if (source && !updatedSources.includes(source)) {
          updatedSources.push(source);
        }
      });
    }
  });
  setSelectedSources(updatedSources);
  if (noPrivateField && !updatedSources.length) {
    setAvailableSources(sources);
  } else {
    setAvailableSources(updatedSources);
  }
};

const fieldValuesToString = (field) => {
  let strValues = '';
  switch (field.type) {
    case 'Numeric':
      field.values.forEach((element) => {
        switch (element.option) {
          case 'between':
            strValues = `${strValues} ${element.value1} <= ${field.name} <= ${element.value2} or `;
            break;
          default:
            strValues = `${strValues} ${field.name} ${element.option} ${element.value1} or `;
        }
      });
      if (strValues.endsWith('or '))
        strValues = strValues.substring(0, strValues.length - 4);
      break;
    case 'Date':
      field.values.forEach((element) => {
        switch (element.option) {
          case 'between':
            strValues = `${strValues} ${element.startDate} <= ${field.name} <= ${element.endDate} or `;
            break;
          default:
            strValues = `${strValues} ${field.name} ${element.option} ${element.startDate} or `;
        }
      });
      if (strValues.endsWith(' or '))
        strValues = strValues.substring(0, strValues.length - 4);
      break;
    case 'List':
      strValues = `${strValues} ${field.name} = `;
      field.values.forEach((element) => {
        strValues = `${strValues} ${element.label}, `;
      });
      if (strValues.endsWith(', '))
        strValues = strValues.substring(0, strValues.length - 2);
      break;
    //type : text
    default:
      strValues = `${strValues} ${field.name} = ${field.values}`;
  }
  return strValues;
};

const updateSearch = (setSearch, searchFields, selectedOperatorId, setSearchCount) => {
  let searchText = '';
  searchFields.forEach((field) => {
    if (field.isValidated) {
      searchText =
        searchText +
        `{${fieldValuesToString(field)} } ${Operators[selectedOperatorId].value.toUpperCase()} `;
    }
  });
  if (searchText.endsWith(' AND ')) {
    searchText = searchText.substring(0, searchText.length - 5);
  } else if (searchText.endsWith(' OR ')) {
    searchText = searchText.substring(0, searchText.length - 4);
  }
  setSearchCount();
  setSearch(searchText);
};

const HistorySelect = ({
  sources,
  setAvailableSources,
  setSelectedSources,
  setSearch,
  setSearchFields,
  setSearchCount,
  setFieldCount,
  userHistory,
  setUserHistory,
}) => {
  const { t } = useTranslation('search');
  const auth = useAuth();
  const client = useGatekeeper();
  const [historySelectError, setHistorySelectError] = useState(undefined);
  const [selectedSavedSearch, setSelectedSavedSearch] = useState(undefined);

  useEffect(() => {
    const fetchHistory = async (sub) => {
      const userHistory = await client.getUserHistory(sub);
      if (userHistory[0] && userHistory[0].ui_structure) {
        userHistory.forEach((item) => {
          item.ui_structure = JSON.parse(item.ui_structure);
          item.label = `${item.name} - ${new Date(item.createdat).toLocaleString()}`;
        });
        setUserHistory(userHistory);
      }
    };
    const sub = auth.userData?.profile?.sub;
    if (sub) {
      fetchHistory(sub);
    }
  }, [auth.userData]);

  const onHistoryChange = (selectedSavedSearch) => {
    setHistorySelectError(undefined);
    if (!selectedSavedSearch) {
      return;
    }
    // if no search is selected
    if (selectedSavedSearch.length <= 0) {
      setSelectedSavedSearch(undefined);
      setSearch('');
      setSearchCount();
      setFieldCount([]);
      setSelectedSources([]);
      setSearchFields([]);
      return;
    }
    if (!!selectedSavedSearch[0].query) {
      setSelectedSavedSearch(selectedSavedSearch);
      setSearch(selectedSavedSearch[0].query);
      setSearchCount();
      setFieldCount([]);
    }
    if (!!selectedSavedSearch[0].ui_structure) {
      updateSources(
        selectedSavedSearch[0].ui_structure,
        sources,
        setSelectedSources,
        setAvailableSources
      );
      setSearchFields(selectedSavedSearch[0].ui_structure);
    }
  };

  const onHistorySearchChange = (value, hasMatchingOptions) => {
    if (value.length === 0 || hasMatchingOptions) {
      setHistorySelectError(undefined);
    } else {
      setHistorySelectError(t('search:advancedSearch.errorInvalidOption', { value }));
    }
  };

  return (
    <>
      {userHistory && Object.keys(userHistory).length !== 0 && (
        <EuiFormRow
          error={historySelectError}
          isInvalid={historySelectError !== undefined}
        >
          <EuiComboBox
            placeholder={t('search:advancedSearch.searchHistory.placeholder')}
            singleSelection={{ asPlainText: true }}
            options={userHistory}
            selectedOptions={selectedSavedSearch}
            onChange={onHistoryChange}
            onSearchChange={onHistorySearchChange}
          />
        </EuiFormRow>
      )}
    </>
  );
};

const SearchBar = ({
  search,
  setSearch,
  setSearchResults,
  searchFields,
  setSearchFields,
  selectedSources,
  setSelectedSources,
  availableSources,
  setAvailableSources,
  standardFields,
  sources,
  setSelectedTabNumber,
  searchCount,
  setSearchCount,
  setFieldCount,
}) => {
  const { t } = useTranslation(['search', 'common']);
  const [isLoading, setIsLoading] = useState(false);
  const [userHistory, setUserHistory] = useState({});
  const [isSaveSearchModalOpen, setIsSaveSearchModalOpen] = useState(false);
  const [readOnlyQuery, setReadOnlyQuery] = useState(true);
  const auth = useAuth();
  const client = useGatekeeper();

  const closeSaveSearchModal = () => {
    setIsSaveSearchModalOpen(false);
  };

  const onClickAdvancedSearch = async () => {
    if (search.trim()) {
      setIsLoading(true);
      const advancedQuery = buildDslQuery(search, standardFields);
      const params = {
        query: advancedQuery,
        sourcesId: availableSources.map((source) => source.id),
        fieldsId: standardFields,
        advancedQuery: true,
      };
      const result = await client.searchQuery(params);
      setSearchResults(result);
      setSelectedTabNumber(1);
      if (isLoading) {
        setIsLoading(false);
      }
    }
  };

  const onClickCountResults = async () => {
    if (!!search) {
      const query = buildDslQuery(search, standardFields);
      const params = {
        query,
        sourcesId: availableSources.map((source) => source.id),
        fieldsId: standardFields,
        advancedQuery: true,
      };
      const result = await client.searchQuery(params);
      if (!result || result.error) {
        toast.error(
          <ToastMessage title={t('validation:error')} message={result.error} />
        );
        setSearchCount(0);
      }
      setSearchCount(result.length);
    }
  };

  const addHistory = async (
    search,
    searchName,
    searchFields,
    searchDescription,
    setUserHistory
  ) => {
    const sub = auth.userData?.profile?.sub;
    if (!sub) {
      return;
    }
    const params = {
      name: searchName,
      query: search,
      ui_structure: JSON.stringify(searchFields),
      description: searchDescription,
    };
    const result = await client.addHistory(sub, params);
    if (result.error) {
      toast.error(<ToastMessage title={t('validation:error')} message={result.error} />);
    } else {
      const userHistory = await client.getUserHistory(sub);
      if (userHistory[0] && userHistory[0].ui_structure) {
        userHistory.forEach((item) => {
          item.ui_structure = JSON.parse(item.ui_structure);
          item.label = `${item.name} - ${new Date(item.createdat).toLocaleString()}`;
        });
        setUserHistory(userHistory);
      }
    }
  };

  const SaveSearchModal = () => {
    const [searchName, setSearchName] = useState('');
    const [searchDescription, setSearchDescription] = useState('');

    const onClickSaveSearch = () => {
      if (!!searchName) {
        addHistory(search, searchName, searchFields, searchDescription, setUserHistory);
        setSearchName('');
        setSearchDescription('');
        closeSaveSearchModal();
      }
    };

    return (
      <EuiOverlayMask>
        <EuiModal onClose={closeSaveSearchModal} initialFocus="[name=searchName]">
          <EuiModalHeader>
            <EuiModalHeaderTitle>
              {t('search:advancedSearch.searchHistory.saveSearch')}
            </EuiModalHeaderTitle>
          </EuiModalHeader>
          <EuiModalBody>
            <EuiForm>
              <EuiFormRow
                label={t('search:advancedSearch.searchHistory.addSavedSearchName')}
              >
                <EuiFieldText
                  name="searchName"
                  value={searchName}
                  onChange={(e) => setSearchName(e.target.value)}
                />
              </EuiFormRow>
              <EuiFormRow
                label={t('search:advancedSearch.searchHistory.addSavedSearchDescription')}
              >
                <EuiTextArea
                  value={searchDescription}
                  onChange={(e) => setSearchDescription(e.target.value)}
                  placeholder={t(
                    'search:advancedSearch.searchHistory.addSavedSearchDescriptionPlaceholder'
                  )}
                  fullWidth
                  compressed
                />
              </EuiFormRow>
            </EuiForm>
          </EuiModalBody>
          <EuiModalFooter>
            <EuiButtonEmpty
              onClick={() => {
                closeSaveSearchModal();
              }}
            >
              {t('common:validationActions.cancel')}
            </EuiButtonEmpty>
            <EuiButton
              onClick={() => {
                onClickSaveSearch();
              }}
              fill
            >
              {t('common:validationActions.save')}
            </EuiButton>
          </EuiModalFooter>
        </EuiModal>
      </EuiOverlayMask>
    );
  };

  const EditableQueryToast = () => {
    return (
      <>
        <EuiTitle>
          <p>{t('search:advancedSearch.editableQueryToast.title')}</p>
        </EuiTitle>
        <EuiSpacer size={'s'} />
        <p>{t('search:advancedSearch.editableQueryToast.content.part1')}</p>
        <EuiSpacer size={'s'} />
        <p>{t('search:advancedSearch.editableQueryToast.content.part2')}</p>
        <EuiSpacer size={'s'} />
        <ul>
          <li>{t('search:advancedSearch.editableQueryToast.content.part3')}</li>
          <EuiSpacer size={'s'} />
          <li>{t('search:advancedSearch.editableQueryToast.content.part4')}</li>
        </ul>
      </>
    );
  };

  return (
    <>
      {isSaveSearchModalOpen && <SaveSearchModal />}
      <EuiFlexGroup>
        <EuiFlexItem>
          <EuiTextArea
            readOnly={readOnlyQuery}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={t('search:advancedSearch.textQueryPlaceholder')}
            fullWidth
          />
        </EuiFlexItem>
        <EuiFlexItem grow={false}>
          <EuiButton
            size="s"
            fill
            onClick={() => {
              onClickAdvancedSearch();
            }}
            isLoading={isLoading}
          >
            {t('search:sendSearchButton')}
          </EuiButton>
          <EuiSpacer size="s" />
          {!isNaN(searchCount) && (
            <>
              <EuiTextColor
                color="secondary"
                style={{ display: 'flex', justifyContent: 'center' }}
              >
                {t('search:advancedSearch.resultsCount', { count: searchCount })}
              </EuiTextColor>
              <EuiSpacer size="s" />
            </>
          )}
          <EuiButton
            size="s"
            onClick={() => {
              onClickCountResults();
            }}
          >
            {t('search:advancedSearch.countResultsButton')}
          </EuiButton>
          <EuiSpacer size="s" />
          <EuiButton
            size="s"
            onClick={() => {
              setIsSaveSearchModalOpen(true);
            }}
          >
            {t('search:advancedSearch.searchHistory.saveSearch')}
          </EuiButton>
          <EuiSpacer size="s" />
          <EuiSwitch
            compressed
            label={t('search:advancedSearch.editableSearchButton')}
            checked={!readOnlyQuery}
            onChange={() => {
              setReadOnlyQuery(!readOnlyQuery);
              if (readOnlyQuery) {
                toast.warning(<EditableQueryToast />, { autoClose: false });
              }
            }}
          />
        </EuiFlexItem>
      </EuiFlexGroup>
      <EuiSpacer size="s" />
      <EuiFlexGroup>
        <EuiFlexItem>
          <HistorySelect
            sources={sources}
            setAvailableSources={setAvailableSources}
            setSelectedSources={setSelectedSources}
            setSearch={setSearch}
            setSearchFields={setSearchFields}
            setSearchCount={setSearchCount}
            setFieldCount={setFieldCount}
            userHistory={userHistory}
            setUserHistory={setUserHistory}
          />
        </EuiFlexItem>
      </EuiFlexGroup>
    </>
  );
};

const PopoverSelect = ({ standardFields, searchFields, setSearchFields }) => {
  const { t } = useTranslation('search');
  const [isPopoverSelectOpen, setIsPopoverSelectOpen] = useState(false);
  const [selectedField, setSelectedField] = useState([]);
  const [selectedSection, setSelectedSection] = useState([]);

  const handleAddField = () => {
    if (!!selectedField[0]) {
      const field = standardFields.find(
        (item) =>
          item.field_name.replace(/_|\./g, ' ') ===
          selectedSection[0].label + ' ' + selectedField[0].label
      );
      switch (field.field_type) {
        case 'Text':
          setSearchFields([
            ...searchFields,
            new SearchField(field.field_name, field.field_type, '', false, field.sources),
          ]);
          break;
        case 'List':
          setSearchFields([
            ...searchFields,
            new SearchField(field.field_name, field.field_type, [], false, field.sources),
          ]);
          break;
        default:
          setSearchFields([
            ...searchFields,
            new SearchField(
              field.field_name,
              field.field_type,
              [{}],
              false,
              field.sources
            ),
          ]);
      }
    }
  };

  const renderOption = (option, searchValue, contentClassName) => {
    const { label, color } = option;
    return <EuiHealth color={color}>{label}</EuiHealth>;
  };

  return (
    <EuiPopover
      panelPaddingSize="s"
      button={
        <EuiButton
          iconType="listAdd"
          iconSide="left"
          onClick={() => setIsPopoverSelectOpen(!isPopoverSelectOpen)}
        >
          {t('search:advancedSearch.fields.addFieldPopover.title')}
        </EuiButton>
      }
      isOpen={isPopoverSelectOpen}
      closePopover={() => setIsPopoverSelectOpen(false)}
    >
      <EuiPopoverTitle style={styles.noBorder} paddingSize={'m'}>
        {t('search:advancedSearch.fields.addFieldPopover.title')}
      </EuiPopoverTitle>
      <EuiFlexGroup direction={'column'} style={{ width: '240px' }}>
        <EuiComboBox
          placeholder={t('search:advancedSearch.fields.addFieldPopover.selectSection')}
          singleSelection={{ asPlainText: true }}
          options={getSections(standardFields)}
          selectedOptions={selectedSection}
          onChange={(selected) => {
            setSelectedSection(selected);
            setSelectedField([]);
          }}
          isClearable={false}
        />
        <EuiComboBox
          placeholder={t('search:advancedSearch.fields.addFieldPopover.selectSection')}
          singleSelection={{ asPlainText: true }}
          options={getFieldsBySection(standardFields, selectedSection[0])}
          selectedOptions={selectedField}
          onChange={(selected) => {
            setSelectedField(selected);
          }}
          isClearable={true}
          renderOption={renderOption}
          isDisabled={selectedSection.length === 0}
        />
      </EuiFlexGroup>
      <EuiPopoverFooter style={styles.noBorder} paddingSize={'m'}>
        <EuiButton
          size="s"
          onClick={() => {
            handleAddField();
            setIsPopoverSelectOpen(false);
            setSelectedSection([]);
            setSelectedField([]);
          }}
        >
          {t('search:advancedSearch.fields.addFieldPopover.validateButton')}
        </EuiButton>
      </EuiPopoverFooter>
    </EuiPopover>
  );
};

const PopoverValueContent = ({
  index,
  standardFields,
  searchFields,
  setSearchFields,
  setSearch,
  setSearchCount,
  fieldCount,
  setFieldCount,
  setIsPopoverValueOpen,
  selectedOperatorId,
  selectedSources,
  setSelectedSources,
  availableSources,
  setAvailableSources,
}) => {
  const { t } = useTranslation(['search', 'common', 'validation']);
  const [valueError, setValueError] = useState(undefined);

  const onValueSearchChange = (value, hasMatchingOptions) => {
    if (value.length === 0 || hasMatchingOptions) {
      setValueError(undefined);
    } else {
      setValueError(t('search:advancedSearch.errorInvalidOption', { value }));
    }
  };

  const PolicyToast = () => {
    return (
      <>
        <EuiTitle size={'xs'}>
          <p>{t('search:advancedSearch.policyToast.title')}</p>
        </EuiTitle>
        <EuiSpacer size={'s'} />
        <p>{t('search:advancedSearch.policyToast.content.0')}</p>
        <EuiSpacer size={'s'} />
        <p>{t('search:advancedSearch.policyToast.content.1')}</p>
      </>
    );
  };

  const validateFieldValues = () => {
    let fieldValues;
    if (Array.isArray(searchFields[index].values)) {
      fieldValues = [];
      searchFields[index].values.forEach((value) => {
        if (!!value) {
          fieldValues.push(value);
        }
      });
    } else {
      fieldValues = searchFields[index].values;
    }

    const updatedSearchFields = updateArrayElement(
      searchFields,
      index,
      new SearchField(
        searchFields[index].name,
        searchFields[index].type,
        fieldValues,
        true,
        searchFields[index].sources
      )
    );
    setSearchFields(updatedSearchFields);
    updateSearch(setSearch, updatedSearchFields, selectedOperatorId, setSearchCount);
    setFieldCount(updateArrayElement(fieldCount, index));
    if (searchFields[index].sources?.length) {
      const filteredSources = [];
      searchFields[index].sources.forEach((sourceId) => {
        let source;
        if (selectedSources.length) {
          source = selectedSources.find((src) => src.id === sourceId);
        } else {
          source = availableSources.find((src) => src.id === sourceId);
        }
        if (source) {
          filteredSources.push(source);
        }
      });
      setAvailableSources(filteredSources);
      setSelectedSources(filteredSources);
      toast.warning(<PolicyToast />, { autoClose: false });
    }
  };

  const invalidateFieldValues = () => {
    const updatedSearchFields = updateArrayElement(
      searchFields,
      index,
      new SearchField(
        searchFields[index].name,
        searchFields[index].type,
        searchFields[index].values,
        false,
        searchFields[index].sources
      )
    );
    setSearchFields(updatedSearchFields);
    updateSearch(setSearch, updatedSearchFields, selectedOperatorId, setSearchCount);
  };

  const ValuePopoverFooter = ({ i }) => {
    if (i === searchFields[index].values.length - 1) {
      return (
        <>
          <EuiButton
            size="s"
            onClick={() => {
              setSearchFields(
                updateArrayElement(
                  searchFields,
                  index,
                  new SearchField(
                    searchFields[index].name,
                    searchFields[index].type,
                    [...searchFields[index].values, {}],
                    false,
                    searchFields[index].sources
                  )
                )
              );
            }}
          >
            {t('search:advancedSearch.fields.fieldContentPopover.addValue')}
          </EuiButton>
          <EuiPopoverFooter style={styles.noBorder}>
            <EuiButton
              size="s"
              style={{ float: 'right' }}
              onClick={() => {
                validateFieldValues();
                setIsPopoverValueOpen(false);
              }}
            >
              {t('common:validationActions.validate')}
            </EuiButton>
          </EuiPopoverFooter>
        </>
      );
    }
  };

  const addFieldValue = (i, selectedOption) => {
    setSearchFields(
      updateSearchFieldValues(
        searchFields,
        index,
        updateArrayElement(searchFields[index].values, i, { option: selectedOption })
      )
    );
  };

  const getListFieldValues = () => {
    const listFieldValues = [];
    standardFields
      .find((item) => item.field_name === searchFields[index].name)
      .values.split(', ')
      .sort()
      .forEach((element) => {
        listFieldValues.push({ label: element });
      });
    return listFieldValues;
  };

  const SelectDates = ({ i }) => {
    if (!!searchFields[index].values[i].option) {
      switch (searchFields[index].values[i].option) {
        case 'between':
          return (
            <EuiFlexItem>
              <EuiFormRow
                label={t('search:advancedSearch.fields.fieldContentPopover.betweenDate')}
              >
                <EuiDatePicker
                  selected={moment(searchFields[index].values[i].startDate)}
                  onChange={(date) =>
                    setSearchFields(
                      updateSearchFieldValues(
                        searchFields,
                        index,
                        updateArrayElement(searchFields[index].values, i, {
                          option: searchFields[index].values[i].option,
                          startDate: date.format('YYYY-MM-DD'),
                          endDate: searchFields[index].values[i].endDate,
                        })
                      )
                    )
                  }
                />
              </EuiFormRow>
              <EuiFormRow
                label={t('search:advancedSearch.fields.fieldContentPopover.andDate')}
              >
                <EuiDatePicker
                  placeholder={t(
                    'search:advancedSearch.fields.fieldContentPopover.andDate'
                  )}
                  selected={moment(searchFields[index].values[i].endDate)}
                  onChange={(date) =>
                    setSearchFields(
                      updateSearchFieldValues(
                        searchFields,
                        index,
                        updateArrayElement(searchFields[index].values, i, {
                          option: searchFields[index].values[i].option,
                          startDate: searchFields[index].values[i].startDate,
                          endDate: date.format('YYYY-MM-DD'),
                        })
                      )
                    )
                  }
                />
              </EuiFormRow>
              <ValuePopoverFooter i={i} />
            </EuiFlexItem>
          );

        default:
          return (
            <EuiFlexItem>
              <EuiFormRow
                label={t(
                  'search:advancedSearch.fields.fieldContentPopover.dateSelection'
                )}
              >
                <EuiDatePicker
                  selected={moment(searchFields[index].values[i].startDate)}
                  onChange={(date) =>
                    setSearchFields(
                      updateSearchFieldValues(
                        searchFields,
                        index,
                        updateArrayElement(searchFields[index].values, i, {
                          option: searchFields[index].values[i].option,
                          startDate: date.format('YYYY-MM-DD'),
                          endDate: moment(),
                        })
                      )
                    )
                  }
                />
              </EuiFormRow>
              <ValuePopoverFooter i={i} />
            </EuiFlexItem>
          );
      }
    }
  };

  const NumericValues = ({ i }) => {
    if (!!searchFields[index].values[i].option) {
      switch (searchFields[index].values[i].option) {
        case 'between':
          return (
            <EuiFlexItem>
              <EuiFormRow
                label={t('search:advancedSearch.fields.fieldContentPopover.firstValue')}
              >
                <EuiFieldText
                  placeholder={t(
                    'search:advancedSearch.fields.fieldContentPopover.inputTextValue'
                  )}
                  value={searchFields[index].values[i].value1}
                  onChange={(e) => {
                    setSearchFields(
                      updateSearchFieldValues(
                        searchFields,
                        index,
                        updateArrayElement(searchFields[index].values, i, {
                          option: searchFields[index].values[i].option,
                          value1: e.target.value,
                          value2: searchFields[index].values[i].value2,
                        })
                      )
                    );
                  }}
                />
              </EuiFormRow>
              <EuiFormRow
                label={t('search:advancedSearch.fields.fieldContentPopover.secondValue')}
              >
                <EuiFieldText
                  placeholder={t(
                    'search:advancedSearch.fields.fieldContentPopover.inputTextValue'
                  )}
                  value={searchFields[index].values[i].value2}
                  onChange={(e) =>
                    setSearchFields(
                      updateSearchFieldValues(
                        searchFields,
                        index,
                        updateArrayElement(searchFields[index].values, i, {
                          option: searchFields[index].values[i].option,
                          value1: searchFields[index].values[i].value1,
                          value2: e.target.value,
                        })
                      )
                    )
                  }
                />
              </EuiFormRow>
              <ValuePopoverFooter i={i} />
            </EuiFlexItem>
          );

        default:
          return (
            <EuiFlexItem>
              <EuiFormRow
                label={t('search:advancedSearch.fields.fieldContentPopover.addValue')}
              >
                <EuiFieldText
                  placeholder={t(
                    'search:advancedSearch.fields.fieldContentPopover.inputTextValue'
                  )}
                  value={searchFields[index].values[i].value1}
                  onChange={(e) => {
                    setSearchFields(
                      updateSearchFieldValues(
                        searchFields,
                        index,
                        updateArrayElement(searchFields[index].values, i, {
                          option: searchFields[index].values[i].option,
                          value1: e.target.value,
                          value2: searchFields[index].values[i].value2,
                        })
                      )
                    );
                  }}
                />
              </EuiFormRow>
              <ValuePopoverFooter i={i} />
            </EuiFlexItem>
          );
      }
    }
  };

  switch (searchFields[index].type) {
    case 'Text':
      return (
        <>
          <EuiFlexItem>
            <EuiFieldText
              placeholder={t(
                'search:advancedSearch.fields.fieldContentPopover.inputTextValue'
              )}
              value={searchFields[index].values}
              onChange={(e) =>
                setSearchFields(
                  updateSearchFieldValues(searchFields, index, e.target.value)
                )
              }
            />
          </EuiFlexItem>
          <EuiPopoverFooter style={styles.noBorder}>
            <EuiButton
              size="s"
              style={{ float: 'right' }}
              onClick={() => {
                validateFieldValues();
                setIsPopoverValueOpen(false);
              }}
            >
              {t('common:validationActions.validate')}
            </EuiButton>
          </EuiPopoverFooter>
        </>
      );
    case 'List':
      return (
        <>
          <EuiFlexItem>
            <EuiFormRow error={valueError} isInvalid={valueError !== undefined}>
              <EuiComboBox
                placeholder={t(
                  'search:advancedSearch.fields.fieldContentPopover.selectValues'
                )}
                options={getListFieldValues()}
                selectedOptions={searchFields[index].values}
                onChange={(selectedOptions) => {
                  setValueError(undefined);
                  setSearchFields(
                    updateSearchFieldValues(searchFields, index, selectedOptions)
                  );
                }}
                onSearchChange={onValueSearchChange}
              />
            </EuiFormRow>
          </EuiFlexItem>
          <EuiPopoverFooter style={styles.noBorder}>
            <EuiButton
              size="s"
              style={{ float: 'right' }}
              onClick={() => {
                validateFieldValues();
                setIsPopoverValueOpen(false);
              }}
            >
              {t('common:validationActions.validate')}
            </EuiButton>
          </EuiPopoverFooter>
        </>
      );
    case 'Numeric':
      return (
        <>
          {searchFields[index].values.map((value, i) => (
            <Fragment key={i}>
              <EuiFormRow
                label={t(
                  'search:advancedSearch.fields.fieldContentPopover.operatorSelection.label'
                )}
              >
                <EuiSelect
                  value={searchFields[index].values[i].option}
                  options={NumericOptions.map((option) => {
                    return { ...option, text: t(option.text) };
                  })}
                  onChange={(e) => {
                    addFieldValue(i, e.target.value);
                    invalidateFieldValues();
                  }}
                  hasNoInitialSelection
                />
              </EuiFormRow>
              <NumericValues i={i} />
            </Fragment>
          ))}
        </>
      );
    case 'Date':
      return (
        <>
          {searchFields[index].values.map((value, i) => (
            <Fragment key={i}>
              <EuiFormRow
                label={t(
                  'search:advancedSearch.fields.fieldContentPopover.operatorSelection.label'
                )}
              >
                <EuiSelect
                  value={searchFields[index].values[i].option}
                  options={DateOptions.map((option) => {
                    return { ...option, text: t(option.text) };
                  })}
                  onChange={(e) => {
                    addFieldValue(i, e.target.value);
                    invalidateFieldValues();
                  }}
                  hasNoInitialSelection
                />
              </EuiFormRow>
              <SelectDates i={i} />
            </Fragment>
          ))}
        </>
      );
    default:
      return <></>;
  }
};

const PopoverValueButton = ({
  index,
  standardFields,
  searchFields,
  setSearchFields,
  setSearch,
  setSearchCount,
  fieldCount,
  setFieldCount,
  selectedOperatorId,
  selectedSources,
  setSelectedSources,
  availableSources,
  setAvailableSources,
}) => {
  const { t } = useTranslation('search');
  const [isPopoverValueOpen, setIsPopoverValueOpen] = useState(false);

  const handleButtonClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setIsPopoverValueOpen((prevState) => !prevState);
  };

  return (
    <EuiPopover
      panelPaddingSize="s"
      button={
        <EuiButtonIcon
          size="s"
          onClick={handleButtonClick}
          iconType="documentEdit"
          title={t('search:advancedSearch.fields.fieldContentPopover.addFieldValues')}
          aria-label={t(
            'search:advancedSearch.fields.fieldContentPopover.addFieldValues'
          )}
        />
      }
      isOpen={isPopoverValueOpen}
      closePopover={() => setIsPopoverValueOpen(false)}
    >
      <EuiFlexGroup direction={'column'} style={{ width: 240 }}>
        <PopoverValueContent
          index={index}
          standardFields={standardFields}
          searchFields={searchFields}
          setSearchFields={setSearchFields}
          setSearch={setSearch}
          setSearchCount={setSearchCount}
          fieldCount={fieldCount}
          setFieldCount={setFieldCount}
          isPopoverValueOpen={isPopoverValueOpen}
          setIsPopoverValueOpen={setIsPopoverValueOpen}
          selectedOperatorId={selectedOperatorId}
          selectedSources={selectedSources}
          setSelectedSources={setSelectedSources}
          availableSources={availableSources}
          setAvailableSources={setAvailableSources}
        />
      </EuiFlexGroup>
    </EuiPopover>
  );
};

const FieldsPanel = ({
  standardFields,
  searchFields,
  setSearchFields,
  setSearch,
  setSearchCount,
  selectedOperatorId,
  fieldCount,
  setFieldCount,
  availableSources,
  setAvailableSources,
  selectedSources,
  setSelectedSources,
  sources,
}) => {
  const { t } = useTranslation('search');
  const client = useGatekeeper();

  const countFieldValues = async (field, index) => {
    const fieldStr = `{${fieldValuesToString(field)}}`;
    const advancedQuery = buildDslQuery(fieldStr, standardFields);
    const params = {
      query: advancedQuery,
      sourcesId: availableSources.map((source) => source.id),
      fieldsId: standardFields,
      advancedQuery: true,
    };
    const result = await client.searchQuery(params);
    const fieldCountUpdated = updateArrayElement(fieldCount, index, result.length);
    setFieldCount(fieldCountUpdated);
  };

  const handleRemoveField = (index) => {
    const updatedSearchFields = removeArrayElement(searchFields, index);
    setSearchFields(updatedSearchFields);
    updateSources(updatedSearchFields, sources, setSelectedSources, setAvailableSources);
    updateSearch(setSearch, updatedSearchFields, selectedOperatorId, setSearchCount);
  };

  const handleClearValues = (index) => {
    let updatedSearchFields;
    switch (searchFields[index].type) {
      case 'Text':
        updatedSearchFields = updateArrayElement(
          searchFields,
          index,
          new SearchField(
            searchFields[index].name,
            searchFields[index].type,
            '',
            false,
            searchFields[index].sources
          )
        );
        break;
      case 'List':
        updatedSearchFields = updateArrayElement(
          searchFields,
          index,
          new SearchField(
            searchFields[index].name,
            searchFields[index].type,
            [],
            false,
            searchFields[index].sources
          )
        );
        break;
      default:
        updatedSearchFields = updateArrayElement(
          searchFields,
          index,
          new SearchField(
            searchFields[index].name,
            searchFields[index].type,
            [{}],
            false,
            searchFields[index].sources
          )
        );
    }
    setSearchFields(updatedSearchFields);
    updateSources(updatedSearchFields, sources, setSelectedSources, setAvailableSources);
    setFieldCount(updateArrayElement(fieldCount, index));
    updateSearch(setSearch, updatedSearchFields, selectedOperatorId, setSearchCount);
  };

  if (standardFields == []) {
    return <h2>{t('search:advancedSearch.fields.loadingFields')}</h2>;
  }

  return (
    <>
      <EuiTitle size="xs">
        <h2>{t('search:advancedSearch.fields.title')}</h2>
      </EuiTitle>
      <EuiPanel hasShadow={false} paddingSize="m">
        <EuiFlexGroup direction="column">
          {searchFields.map((field, index) => (
            <EuiPanel
              hasShadow={false}
              hasBorder={true}
              key={'field' + index}
              paddingSize="s"
            >
              <EuiFlexGroup direction="row" alignItems="center">
                <EuiFlexItem grow={false}>
                  <EuiButtonIcon
                    size="s"
                    color="danger"
                    onClick={() => handleRemoveField(index)}
                    iconType="indexClose"
                    title={t('search:advancedSearch.fields.removeFieldButton')}
                    aria-label={t('search:advancedSearch.fields.removeFieldButton')}
                  />
                </EuiFlexItem>
                <EuiFlexItem>
                  {field.isValidated ? (
                    <>
                      {field.sources?.length ? (
                        <EuiHealth color="danger">
                          {fieldValuesToString(field).replace(/_|\./g, ' ')}
                        </EuiHealth>
                      ) : (
                        <EuiHealth color="primary">
                          {fieldValuesToString(field).replace(/_|\./g, ' ')}
                        </EuiHealth>
                      )}
                    </>
                  ) : (
                    <>
                      {field.sources?.length ? (
                        <EuiHealth color="danger">
                          {field.name.replace(/_|\./g, ' ')}
                        </EuiHealth>
                      ) : (
                        <EuiHealth color="primary">
                          {field.name.replace(/_|\./g, ' ')}
                        </EuiHealth>
                      )}
                    </>
                  )}
                </EuiFlexItem>
                {!isNaN(fieldCount[index]) && (
                  <EuiFlexItem grow={false}>
                    <EuiTextColor color="secondary">
                      {t('search:advancedSearch.resultsCount', {
                        count: fieldCount[index],
                      })}
                    </EuiTextColor>
                  </EuiFlexItem>
                )}
                {field.isValidated && (
                  <EuiFlexItem grow={false}>
                    <EuiButtonIcon
                      size="s"
                      onClick={() => countFieldValues(field, index)}
                      iconType="number"
                      title={t('search:advancedSearch.countResultsButton')}
                      aria-label={t('search:advancedSearch.countResultsButton')}
                    />
                  </EuiFlexItem>
                )}
                {field.isValidated && (
                  <EuiFlexItem grow={false}>
                    <EuiButtonIcon
                      size="s"
                      color="danger"
                      onClick={() => handleClearValues(index)}
                      iconType="trash"
                      title={t('search:advancedSearch.fields.clearValues')}
                      aria-label={t('search:advancedSearch.fields.clearValues')}
                    />
                  </EuiFlexItem>
                )}
                <EuiFlexItem grow={false}>
                  <PopoverValueButton
                    index={index}
                    standardFields={standardFields}
                    searchFields={searchFields}
                    setSearchFields={setSearchFields}
                    setSearch={setSearch}
                    setSearchCount={setSearchCount}
                    fieldCount={fieldCount}
                    setFieldCount={setFieldCount}
                    selectedOperatorId={selectedOperatorId}
                    selectedSources={selectedSources}
                    setSelectedSources={setSelectedSources}
                    availableSources={availableSources}
                    setAvailableSources={setAvailableSources}
                  />
                </EuiFlexItem>
              </EuiFlexGroup>
            </EuiPanel>
          ))}
        </EuiFlexGroup>
        {searchFields.length > 0 && <EuiSpacer size="l" />}
        <PopoverSelect
          standardFields={standardFields}
          searchFields={searchFields}
          setSearchFields={setSearchFields}
        />
      </EuiPanel>
    </>
  );
};

const SearchOperatorSelection = ({
  setSearch,
  searchFields,
  setSearchCount,
  selectedOperatorId,
  setSelectedOperatorId,
}) => {
  const { t } = useTranslation('search');

  return (
    <EuiRadioGroup
      options={Operators.map((operator) => {
        return { ...operator, label: t(operator.label) };
      })}
      idSelected={selectedOperatorId}
      onChange={(id) => {
        setSelectedOperatorId(id);
        updateSearch(setSearch, searchFields, id, setSearchCount);
      }}
      name="operators group"
      legend={{
        children: <span>{t('search:advancedSearch.searchOptions.title')}</span>,
      }}
    />
  );
};

const SourceSelect = ({ availableSources, selectedSources, setSelectedSources }) => {
  const { t } = useTranslation('search');
  const [sourceSelectError, setSourceSelectError] = useState(undefined);

  if (Object.keys(availableSources).length === 0) {
    return (
      <p>
        <EuiIcon type="alert" color="danger" />
      </p>
    );
  }
  availableSources.forEach((source) => {
    if (source.name) {
      source = changeNameToLabel(source);
    }
  });

  const onSourceChange = (selectedOptions) => {
    setSourceSelectError(undefined);
    setSelectedSources(selectedOptions);
  };

  const onSourceSearchChange = (value, hasMatchingOptions) => {
    if (value.length === 0 || hasMatchingOptions) {
      setSourceSelectError(undefined);
    } else {
      setSourceSelectError(
        t('search:advancedSearch.errorInvalidOption', { value: value })
      );
    }
  };

  return (
    <>
      <EuiTitle size="xs">
        <h2>{t('search:advancedSearch.partnerSources.title')}</h2>
      </EuiTitle>
      <EuiSpacer size="s" />
      <EuiFormRow error={sourceSelectError} isInvalid={sourceSelectError !== undefined}>
        <EuiComboBox
          placeholder={t('search:advancedSearch.partnerSources.allSourcesSelected')}
          options={availableSources}
          selectedOptions={selectedSources}
          onChange={onSourceChange}
          onSearchChange={onSourceSearchChange}
        />
      </EuiFormRow>
    </>
  );
};

const AdvancedSearch = ({
  search,
  setSearch,
  setSearchResults,
  selectedSources,
  setSelectedSources,
  availableSources,
  setAvailableSources,
  standardFields,
  setStandardFields,
  sources,
  setSelectedTabNumber,
  setIsAdvancedSearch,
  isAdvancedSearch,
}) => {
  const [selectedOperatorId, setSelectedOperatorId] = useState('0');
  const [searchFields, setSearchFields] = useState([]);
  const [fieldCount, setFieldCount] = useState([]);
  const [searchCount, setSearchCount] = useState();

  return (
    <>
      <SearchModeSwitcher
        isAdvancedSearch={isAdvancedSearch}
        setIsAdvancedSearch={setIsAdvancedSearch}
      />
      <EuiFlexGroup>
        <EuiFlexItem>
          <EuiSpacer size="s" />
          <SearchBar
            search={search}
            setSearch={setSearch}
            setSearchResults={setSearchResults}
            searchFields={searchFields}
            setSearchFields={setSearchFields}
            selectedSources={selectedSources}
            setSelectedSources={setSelectedSources}
            availableSources={availableSources}
            setAvailableSources={setAvailableSources}
            standardFields={standardFields}
            sources={sources}
            setSelectedTabNumber={setSelectedTabNumber}
            searchCount={searchCount}
            setSearchCount={setSearchCount}
            setFieldCount={setFieldCount}
          />
        </EuiFlexItem>
      </EuiFlexGroup>
      <EuiFlexGroup>
        <EuiFlexItem>
          <EuiSpacer size="s" />
          <FieldsPanel
            standardFields={standardFields}
            setStandardFields={setStandardFields}
            searchFields={searchFields}
            setSearchFields={setSearchFields}
            search={search}
            setSearch={setSearch}
            setSearchCount={setSearchCount}
            selectedOperatorId={selectedOperatorId}
            setSelectedOperatorId={setSelectedOperatorId}
            fieldCount={fieldCount}
            setFieldCount={setFieldCount}
            availableSources={availableSources}
            setAvailableSources={setAvailableSources}
            selectedSources={selectedSources}
            setSelectedSources={setSelectedSources}
            sources={sources}
          />
          <SearchOperatorSelection
            setSearch={setSearch}
            searchFields={searchFields}
            setSearchCount={setSearchCount}
            selectedOperatorId={selectedOperatorId}
            setSelectedOperatorId={setSelectedOperatorId}
          />
          <EuiSpacer size="s" />
          <SourceSelect
            availableSources={availableSources}
            selectedSources={selectedSources}
            setSelectedSources={setSelectedSources}
          />
        </EuiFlexItem>
      </EuiFlexGroup>
    </>
  );
};

export default AdvancedSearch;
