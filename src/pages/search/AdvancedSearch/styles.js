const styles = {
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  noBorder: {
    border: 'none',
  },
};

export default styles;
