export const Operators = [
  {
    id: '0',
    value: 'And',
    label: 'search:advancedSearch.searchOptions.matchAll',
  },
  {
    id: '1',
    value: 'Or',
    label: 'search:advancedSearch.searchOptions.matchAtLeastOne',
  },
];

export const DateOptions = [
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.date.value',
    value: '=',
  },
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.date.before',
    value: '<=',
  },
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.date.after',
    value: '>=',
  },
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.date.range',
    value: 'between',
  },
];

export const NumericOptions = [
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.numeric.value',
    value: '=',
  },
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.numeric.under',
    value: '<=',
  },
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.numeric.over',
    value: '>=',
  },
  {
    text: 'search:advancedSearch.fields.fieldContentPopover.operatorSelection.numeric.range',
    value: 'between',
  },
];
