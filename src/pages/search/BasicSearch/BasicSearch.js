import React, { useState } from 'react';
import {
  EuiButton,
  EuiFieldSearch,
  EuiFlexGroup,
  EuiFlexItem,
  EuiSpacer,
} from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import SearchModeSwitcher from '../SearchModeSwitcher';
import { useGatekeeper } from '../../../contexts/GatekeeperContext';

const BasicSearch = ({
  standardFields,
  availableSources,
  basicSearch,
  setBasicSearch,
  setIsAdvancedSearch,
  isAdvancedSearch,
  setSearchResults,
  setSelectedTabNumber,
}) => {
  const { t } = useTranslation('search');
  const client = useGatekeeper();
  const [isLoading, setIsLoading] = useState(false);

  const onFormSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const result = await client.searchQuery({
      query: basicSearch,
      sourcesId: availableSources.map((source) => source.id),
      fieldsId: standardFields,
    });
    setIsLoading(false);
    setSearchResults(result);
    setSelectedTabNumber(1);
  };

  return (
    <>
      <SearchModeSwitcher
        isAdvancedSearch={isAdvancedSearch}
        setIsAdvancedSearch={setIsAdvancedSearch}
      />
      <EuiFlexGroup>
        <EuiFlexItem>
          <EuiSpacer size="s" />
          <form onSubmit={onFormSubmit}>
            <EuiFlexGroup>
              <EuiFlexItem>
                <EuiFieldSearch
                  value={basicSearch}
                  onChange={(e) => setBasicSearch(e.target.value)}
                  placeholder={t('search:basicSearch.searchInputPlaceholder')}
                  autoFocus={true}
                  fullWidth
                  isLoading={isLoading}
                />
              </EuiFlexItem>
              <EuiFlexItem grow={false}>
                <EuiButton type="submit" fill isDisabled={isAdvancedSearch}>
                  {t('search:sendSearchButton')}
                </EuiButton>
              </EuiFlexItem>
            </EuiFlexGroup>
          </form>
        </EuiFlexItem>
      </EuiFlexGroup>
    </>
  );
};

export default BasicSearch;
