import React from 'react';
import { useTranslation } from 'react-i18next';
import { EuiButtonEmpty, EuiFlexGroup, EuiFlexItem } from '@elastic/eui';

const SearchModeSwitcher = ({ isAdvancedSearch, setIsAdvancedSearch }) => {
  const { t } = useTranslation('search');

  const buildSwitcherText = () => {
    if (!isAdvancedSearch) {
      return t('search:basicSearch.switchSearchMode');
    } else {
      return t('search:advancedSearch.switchSearchMode');
    }
  };

  return (
    <EuiFlexGroup>
      <EuiFlexItem grow={false}>
        <EuiButtonEmpty
          onClick={() => {
            setIsAdvancedSearch(!isAdvancedSearch);
          }}
        >
          {buildSwitcherText()}
        </EuiButtonEmpty>
      </EuiFlexItem>
    </EuiFlexGroup>
  );
};

export default SearchModeSwitcher;
