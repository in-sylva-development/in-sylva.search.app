import React, { useState, useEffect } from 'react';
import { EuiTabbedContent, EuiFlexGroup, EuiFlexItem, EuiSpacer } from '@elastic/eui';
import Results from '../results/Results';
import SearchMap from '../maps/SearchMap';
import { useTranslation } from 'react-i18next';
import AdvancedSearch from './AdvancedSearch/AdvancedSearch';
import BasicSearch from './BasicSearch/BasicSearch';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import ResourceFlyout from '../results/ResourceFlyout/ResourceFlyout';

const Search = () => {
  const client = useGatekeeper();
  const { t } = useTranslation('search');
  const [selectedTabNumber, setSelectedTabNumber] = useState(0);
  const [isAdvancedSearch, setIsAdvancedSearch] = useState(false);
  const [selectedSources, setSelectedSources] = useState([]);
  const [sources, setSources] = useState([]);
  const [availableSources, setAvailableSources] = useState([]);
  const [search, setSearch] = useState('');
  const [basicSearch, setBasicSearch] = useState('');
  const [standardFields, setStandardFields] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const [selectedResultsRowsIds, setSelectedResultsRowsIds] = useState([]);
  const [isResourceFlyoutOpen, setIsResourceFlyoutOpen] = useState(false);
  const [resourceFlyoutData, setResourceFlyoutData] = useState({});

  useEffect(() => {
    if (!client) {
      return;
    }
    const fetchElements = async () => {
      const publicFields = await client.getPublicFields();
      setStandardFields(publicFields);
      const sources = await client.getSources();
      setSources(sources);
      setAvailableSources(sources);
    };
    fetchElements();
    // client
    //   .getPublicFields()
    //   .then((resultStdFields) => {
    //     resultStdFields.forEach((field) => {
    //       field.sources = [];
    //     });
    //     setStandardFields(resultStdFields);
    //     fetchUserPolicyFields(sessionStorage.getItem('kcId')).then(
    //       (resultPolicyFields) => {
    //         const userFields = resultStdFields;
    //         resultPolicyFields.forEach((polField) => {
    //           const stdFieldIndex = userFields.findIndex(
    //             (stdField) => stdField.id === polField.std_id
    //           );
    //           if (stdFieldIndex >= 0) {
    //             if (!userFields[stdFieldIndex].sources.includes(polField.source_id))
    //               userFields[stdFieldIndex].sources.push(polField.source_id);
    //           } else {
    //             const newField = {
    //               id: polField.std_id,
    //               sources: [polField.source_id],
    //               ...polField,
    //             };
    //             userFields.push(newField);
    //           }
    //         });
    //         userFields.sort((a, b) => (a.id > b.id ? 1 : b.id > a.id ? -1 : 0));
    //         setStandardFields(removeNullFields(userFields));
    //       }
    //     );
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
    // fetchSources(sessionStorage.getItem('kcId'))
    //   .then((result) => {
    //     setSources(result);
    //     setAvailableSources(result);
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
  }, []);

  // On new search, reset selected rows
  useEffect(() => {
    setSelectedResultsRowsIds([]);
  }, [searchResults]);

  // Returns all data from a resource from its id. Used to display resource data in flyout
  const getResourceDataFromRowId = (id) => {
    return searchResults.find((resource) => resource.id === id);
  };

  // Add data to resource flyout, so it can be displayed.
  const setResourceFlyoutDataFromId = (id) => {
    const resourceData = getResourceDataFromRowId(id);
    // Extract all values except for id to avoid displaying it to user
    setResourceFlyoutData((({ id, ...rest }) => rest)(resourceData));
  };

  const tabsContent = [
    {
      id: 'tab1',
      name: t('search:tabs.composeSearch'),
      content: (
        <EuiFlexGroup>
          <EuiFlexItem>
            <EuiSpacer size={'l'} />
            {isAdvancedSearch ? (
              <AdvancedSearch
                search={search}
                setSearch={setSearch}
                setSearchResults={setSearchResults}
                selectedSources={selectedSources}
                setSelectedSources={setSelectedSources}
                availableSources={availableSources}
                setAvailableSources={setAvailableSources}
                standardFields={standardFields}
                setStandardFields={setStandardFields}
                sources={sources}
                setSelectedTabNumber={setSelectedTabNumber}
                isAdvancedSearch={isAdvancedSearch}
                setIsAdvancedSearch={setIsAdvancedSearch}
              />
            ) : (
              <BasicSearch
                isAdvancedSearch={isAdvancedSearch}
                setIsAdvancedSearch={setIsAdvancedSearch}
                standardFields={standardFields}
                availableSources={availableSources}
                basicSearch={basicSearch}
                setBasicSearch={setBasicSearch}
                setSearchResults={setSearchResults}
                setSelectedTabNumber={setSelectedTabNumber}
              />
            )}
          </EuiFlexItem>
        </EuiFlexGroup>
      ),
    },
    {
      id: 'tab2',
      name: t('search:tabs.results'),
      content: (
        <EuiFlexGroup>
          <EuiFlexItem>
            <EuiSpacer size="l" />
            <Results
              searchResults={searchResults}
              searchQuery={isAdvancedSearch ? search : basicSearch}
              selectedRowsIds={selectedResultsRowsIds}
              setSelectedRowsIds={setSelectedResultsRowsIds}
              setResourceFlyoutDataFromId={setResourceFlyoutDataFromId}
              setIsResourceFlyoutOpen={setIsResourceFlyoutOpen}
            />
          </EuiFlexItem>
        </EuiFlexGroup>
      ),
    },
    {
      id: 'tab3',
      name: t('search:tabs.map'),
      content: (
        <EuiFlexGroup>
          <EuiFlexItem>
            <EuiSpacer size="l" />
            <SearchMap
              searchResults={searchResults}
              selectedPointsIds={selectedResultsRowsIds}
              setSelectedPointsIds={setSelectedResultsRowsIds}
              setResourceFlyoutDataFromId={setResourceFlyoutDataFromId}
              setIsResourceFlyoutOpen={setIsResourceFlyoutOpen}
            />
          </EuiFlexItem>
        </EuiFlexGroup>
      ),
    },
  ];

  return (
    <>
      <ResourceFlyout
        resourceFlyoutData={resourceFlyoutData}
        setResourceFlyoutData={setResourceFlyoutData}
        isResourceFlyoutOpen={isResourceFlyoutOpen}
        setIsResourceFlyoutOpen={setIsResourceFlyoutOpen}
      />
      <EuiTabbedContent
        tabs={tabsContent}
        selectedTab={tabsContent[selectedTabNumber]}
        onTabClick={(tab) => {
          setSelectedTabNumber(tabsContent.indexOf(tab));
        }}
      />
    </>
  );
};

export default Search;
