import React from 'react';
import { EuiCallOut, EuiFlexGroup, EuiFlexItem, EuiSpacer } from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import ResultsTableMUI from './ResultsTableMUI';
import ResultsDownload from './ResultsDownload';

const Results = ({
  searchResults,
  searchQuery,
  selectedRowsIds,
  setSelectedRowsIds,
  setResourceFlyoutDataFromId,
  setIsResourceFlyoutOpen,
}) => {
  const { t } = useTranslation('results');

  return (
    <>
      <EuiFlexGroup>
        <EuiFlexItem>
          <EuiCallOut size="s" title={t('results:clickOnRowTip')} iconType="search" />
        </EuiFlexItem>
        <EuiFlexItem grow={false}>
          <ResultsDownload
            query={searchQuery}
            searchResults={searchResults}
            selectedRowsIds={selectedRowsIds}
          />
        </EuiFlexItem>
      </EuiFlexGroup>
      <EuiSpacer size={'m'} />
      <ResultsTableMUI
        searchResults={searchResults}
        searchQuery={searchQuery}
        selectedRowsIds={selectedRowsIds}
        setSelectedRowsIds={setSelectedRowsIds}
        setResourceFlyoutDataFromId={setResourceFlyoutDataFromId}
        setIsResourceFlyoutOpen={setIsResourceFlyoutOpen}
      />
    </>
  );
};

export default Results;
