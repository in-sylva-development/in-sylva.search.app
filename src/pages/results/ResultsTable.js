import React, { useState } from 'react';
import { EuiBasicTable } from '@elastic/eui';

const buildColumnName = (name) => {
  name = name.split('.').join(' ');
  name = name.charAt(0).toUpperCase() + name.slice(1);
  return name;
};

/*
 FILE NOT IN USE.
 Created to try removing MUI results table, allowing to remove all MUI packages from project.
 Keeping it for now just in case.
 */
const ResultsTable = ({ searchResults }) => {
  const [pageIndex, setPageIndex] = useState(0);
  const [pageSize, setPageSize] = useState(10);

  function getValueFromPath(obj, path) {
    return path.split('.').reduce((acc, part, i, arr) => {
      if (Array.isArray(acc)) {
        // The next part should be a key to match within array objects
        const key = arr[i + 1];
        if (key) {
          const found = acc.find((item) => item.type === key);
          arr.splice(i + 1, 1); // Remove the key from the path array
          return found ? found.value : undefined;
        }
      }
      return acc && acc[part];
    }, obj);
  }

  function getValues(obj, paths) {
    return paths.reduce((acc, path) => {
      acc[path] = getValueFromPath(obj, path);
      return acc;
    }, {});
  }

  const buildRows = (results, columns) => {
    let dataRows = [];
    results.forEach((result) => {
      let row = getValues(result, columns);
      dataRows.push(row);
    });
    return dataRows;
  };

  const buildColumns = (userColumns) => {
    let dataColumns = [];
    dataColumns.push({
      field: 'isRowOpen',
      name: 'Open',
      sortable: false,
      truncateText: true,
    });
    userColumns.forEach((userColumn) => {
      dataColumns.push({
        field: userColumn,
        name: buildColumnName(userColumn),
        sortable: true,
        truncateText: true,
      });
    });
    return dataColumns;
  };

  const buildSearchResultsTable = (results) => {
    if (!results && results.length > 0) {
      return;
    }
    const userColumns = [
      'id',
      'resource.title',
      'resource.lineage',
      'resource.identifier',
      'resource.date.publication',
      'resource.date.revision',
      'resource.INSPIRE_type',
    ];
    const columns = buildColumns(userColumns);
    const rows = buildRows(results, userColumns);
    return {
      rows,
      columns,
    };
  };

  const { rows, columns } = buildSearchResultsTable(searchResults);

  const paginationOptions = {
    pageSizeOptions: [5, 10, 20, 0],
    pageIndex,
    pageSize,
    totalItemCount: rows?.length,
  };

  const onTableChange = ({ page }) => {
    if (page) {
      setPageIndex(page.index);
      setPageSize(page.size);
    }
  };

  return (
    <EuiBasicTable
      items={rows}
      columns={columns}
      pagination={paginationOptions}
      onChange={onTableChange}
    />
  );
};

export default ResultsTable;
