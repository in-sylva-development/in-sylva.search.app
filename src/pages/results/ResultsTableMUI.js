import React, { useEffect, useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import MUIDataTable from 'mui-datatables';
import { createTheme, ThemeProvider } from '@mui/material';
import { buildFieldName } from '../../Utils';
import { useGatekeeper } from '../../contexts/GatekeeperContext';
import { useUserInfo } from '../../contexts/TokenContext';
import { makeStyles } from '@mui/styles';
import { useEuiTheme, transparentize, EuiTitle } from '@elastic/eui';

const useStyles = makeStyles(() => ({
  centeredHeader: {
    '& span': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
}));

const ResultsTableMUI = ({
  searchResults,
  searchQuery,
  selectedRowsIds,
  setSelectedRowsIds,
  setIsResourceFlyoutOpen,
  setResourceFlyoutDataFromId,
}) => {
  const { t } = useTranslation('results');
  const client = useGatekeeper();
  const getUserInfo = useUserInfo();
  const { euiTheme } = useEuiTheme();
  const classes = useStyles();
  const [publicFields, setPublicFields] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [rowsPerPage, setRowsPerPage] = useState(15);
  const [rows, setRows] = useState([]);
  const [columns, setColumns] = useState([]);

  useEffect(() => {
    const fetchFieldsForUser = async () => {
      const publicFields = await client.getPublicFields(); // TODO: get also private fields that user has access to
      setPublicFields(publicFields);
    };
    const getUserFieldsDisplaySettings = async () => {
      const userInfo = await getUserInfo();
      const userFields = await client.getUserFieldsDisplaySettings(userInfo.sub);
      if (userFields && userFields.length > 0) {
        const columns = buildColumns(userFields);
        setColumns(columns);
        const rows = buildRows(searchResults, columns);
        setRows(rows);
        setIsLoading(false);
      }
    };
    if (!searchResults || searchResults.length === 0 || searchResults.error) {
      return;
    }
    fetchFieldsForUser();
    getUserFieldsDisplaySettings();
  }, [searchResults]);

  const buildColumns = (userFields) => {
    let columns = [
      {
        name: 'id',
        label: 'ID',
        options: { display: 'excluded' },
      },
    ];
    // Add to columns the standard fields given as parameter
    // Retrieve
    userFields.forEach((field) => {
      columns.push({
        name: field.field_name,
        label: buildFieldName(field.field_name),
        options: {
          display: true,
          // Apply styling on columns headers text
          customHeadLabelRender: (columnMeta) => {
            return (
              <EuiTitle size={'xxs'}>
                <p>{columnMeta.label}</p>
              </EuiTitle>
            );
          },
          // Truncate text to avoid oversize rows
          customBodyRenderLite: (dataIndex, rowIndex) => {
            let value = rows[rowIndex][field.field_name];
            if (value && value.length >= 150) {
              value = value.substring(0, 150) + ' ...';
            }
            return value;
          },
          // Apply a max width on table cells
          setCellProps: () => ({
            style: {
              maxWidth: '350px',
              textOverflow: 'ellipsis',
            },
          }),
        },
      });
    });
    columns.sort((a, b) => (a.id < b.id ? -1 : a.id > b.id ? 1 : 0));
    return columns;
  };

  const buildRows = (results, columns) => {
    return results.map((result) => {
      let row = { id: result.id };
      columns.forEach((column) => {
        const value = getValueByPath(result, column.name);
        row[column.name] = typeof value === 'string' ? value : value?.toString();
      });
      return row;
    });
  };

  // Returns value from JSON obj associated to key string.
  const getValueByPath = (obj, path) => {
    return path.split('.').reduce((acc, key) => {
      if (Array.isArray(acc)) {
        // For objects with cardinality, defaults to the first element of the array
        // TODO add a way to view other elements of the array when possible.
        acc = acc[0];
      }
      return acc && acc[key];
    }, obj);
  };

  // Add row to list of selected on checkbox click
  const onRowSelectionCallback = (currentRowsSelected, allRowsSelected, rowsSelected) => {
    setSelectedRowsIds(rowsSelected.map((index) => rows[index].id));
  };

  // Open resource flyout on row click (any cell)
  const onCellClickCallback = (cellData, cellState) => {
    if (!searchResults || searchResults.length === 0) {
      return;
    }
    setResourceFlyoutDataFromId(rows[cellState.dataIndex].id);
    setIsResourceFlyoutOpen(true);
  };

  // Remove the delete rows button
  const CustomSelectToolbar = () => {
    return <></>;
  };

  // Translations for mui-datatables component texts
  const textLabels = {
    body: {
      noMatch: t('results:table.textLabels.body.noMatch'),
      toolTip: t('results:table.textLabels.body.toolTip'),
    },
    pagination: {
      next: t('results:table.textLabels.pagination.next'),
      previous: t('results:table.textLabels.pagination.previous'),
      rowsPerPage: t('results:table.textLabels.pagination.rowsPerPage'),
      displayRows: t('results:table.textLabels.pagination.displayRows'),
      jumpToPage: t('results:table.textLabels.pagination.jumpToPage'),
    },
    toolbar: {
      search: t('results:table.textLabels.toolbar.search'),
      viewColumns: t('results:table.textLabels.toolbar.viewColumns'),
      filterTable: t('results:table.textLabels.toolbar.filterTable'),
    },
    filter: {
      all: t('results:table.textLabels.filter.all'),
      title: t('results:table.textLabels.filter.title'),
      reset: t('results:table.textLabels.filter.reset'),
    },
    viewColumns: {
      title: t('results:table.textLabels.viewColumns.title'),
      titleAria: t('results:table.textLabels.viewColumns.titleAria'),
    },
    selectedRows: {
      text: t('results:table.textLabels.selectedRows.text'),
    },
  };

  const getMuiTheme = () =>
    createTheme({
      components: {
        MUIDataTableBodyRow: {
          styleOverrides: {
            root: {
              '&:nth-of-type(odd)': {
                backgroundColor: transparentize(euiTheme.colors.lightShade, 0.5),
              },
            },
          },
        },
        MuiTableRow: {
          styleOverrides: {
            root: {
              '&:hover': {
                cursor: 'pointer',
              },
            },
          },
        },
      },
    });

  const tableOptions = {
    print: false,
    download: false,
    filter: true,
    filterType: 'textField',
    responsive: 'standard',
    draggableColumns: {
      enabled: true,
      transitionTime: 200,
    },
    selectableRows: 'multiple',
    selectableRowsOnClick: false,
    rowsSelected: rows
      .filter((row) => selectedRowsIds.includes(row.id))
      .map((row) => rows.indexOf(row)),
    rowsPerPage: rowsPerPage,
    onChangeRowsPerPage: (newRowsPerPage) => {
      setRowsPerPage(newRowsPerPage);
    },
    rowsPerPageOptions: [15, 30, 50, 100, 250],
    jumpToPage: true,
    searchPlaceholder: t('results:table.search'),
    elevation: 0,
    customToolbarSelect: () => <CustomSelectToolbar />,
    selectToolbarPlacement: 'above',
    onRowSelectionChange: onRowSelectionCallback,
    onCellClick: onCellClickCallback,
    textLabels,
  };

  const isTableReady = !isLoading && columns.length > 0 && rows.length > 0;

  return (
    <ThemeProvider theme={getMuiTheme()}>
      {isTableReady && (
        <MUIDataTable
          title={<Trans i18nKey={'results:table.title'} components={{ searchQuery }} />}
          data={rows}
          columns={columns}
          options={tableOptions}
        />
      )}
    </ThemeProvider>
  );
};

export default ResultsTableMUI;
