import React, { useState } from 'react';
import { EuiButton, EuiFlexGroup, EuiSelect, EuiToolTip } from '@elastic/eui';
import download from 'downloadjs';
import { useTranslation } from 'react-i18next';

const ResultsDownload = ({ query, searchResults, selectedRowsIds }) => {
  const { t } = useTranslation('results');
  const [resultsRowsSelection, setResultsRowsSelection] = useState('all');
  const [format, setFormat] = useState('json');

  const downloadJSON = (content) => {
    download(
      new Blob([
        `{"query": ${JSON.stringify(query)}, "metadataRecords": ${JSON.stringify(content, null, '\t')}}`,
      ]),
      `InSylvaSearchResults.json`,
      'application/json;charset=utf-8'
    );
  };

  const JSONtoCSV = (jsonData) => {
    let csv = '';
    let headers = Object.keys(jsonData[0]);
    csv += headers.join(',') + '\n';
    jsonData.forEach((row) => {
      let data = headers.map((header) => JSON.stringify(row[header])).join(',');
      csv += data + '\n';
    });
    return csv;
  };

  const downloadCSV = (content) => {
    download(JSONtoCSV(content), `InSylvaSearchResults.csv`, 'text/csv');
  };

  const downloadResults = () => {
    if (!searchResults) {
      return;
    }
    let downloadResults = searchResults;
    // Change content to download if user selected 'selected results only'
    if (resultsRowsSelection !== 'all') {
      downloadResults = searchResults.filter((result) => {
        return selectedRowsIds.find((id) => result.id === id);
      });
    }
    // Switch will ease the need to add new formats in the future.
    switch (format) {
      case 'json':
        downloadJSON(downloadResults);
        break;
      case 'csv':
        downloadCSV(downloadResults);
        break;
      default:
        downloadJSON(downloadResults);
        break;
    }
  };

  const ResultsRowSelectionSelectable = () => {
    const resultsRowSelectionOptions = [
      {
        value: 'all',
        text: t('results:downloadResultsButtons.selection.all'),
      },
      {
        value: 'selected',
        text: t('results:downloadResultsButtons.selection.selectionOnly'),
        disabled: selectedRowsIds.length <= 0,
      },
    ];

    const onResultsRowSelectionChange = (e) => {
      setResultsRowsSelection(e.target.value);
    };

    return (
      <EuiToolTip
        position="top"
        content={t('results:downloadResultsButtons.selection.label')}
      >
        <EuiSelect
          value={resultsRowsSelection}
          options={resultsRowSelectionOptions}
          onChange={onResultsRowSelectionChange}
          style={{
            borderRadius: '0',
          }}
          aria-label={t('results:downloadResultsButtons.selection.label')}
          compressed
        />
      </EuiToolTip>
    );
  };

  const FormatSelection = () => {
    const resultsRowSelectionOptions = [
      {
        value: 'json',
        text: t('results:downloadResultsButtons.format.JSON'),
      },
      {
        value: 'csv',
        text: t('results:downloadResultsButtons.format.CSV'),
      },
    ];

    const onFormatChange = (e) => {
      setFormat(e.target.value);
    };

    return (
      <EuiToolTip
        position="top"
        content={t('results:downloadResultsButtons.format.label')}
      >
        <EuiSelect
          value={format}
          options={resultsRowSelectionOptions}
          onChange={onFormatChange}
          aria-label={t('results:downloadResultsButtons.format.label')}
          style={{
            borderTopLeftRadius: '0',
            borderBottomLeftRadius: '0',
          }}
          compressed
        />
      </EuiToolTip>
    );
  };

  return (
    <EuiFlexGroup gutterSize={'none'}>
      <EuiButton
        style={{ borderBottomRightRadius: '0', borderTopRightRadius: '0' }}
        fill
        size={'s'}
        onClick={() => downloadResults()}
      >
        {t('results:downloadResultsButtons.download')}
      </EuiButton>
      <ResultsRowSelectionSelectable />
      <FormatSelection />
    </EuiFlexGroup>
  );
};

export default ResultsDownload;
