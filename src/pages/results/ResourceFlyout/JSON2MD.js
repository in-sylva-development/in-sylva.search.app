import standard_markdown_styles from './standard_markdown_styles_FR_EN.json';

const DEFAULT_ARRAY_SEPARATOR = ',';

const handleString = (
  key,
  value,
  styles,
  lang,
  indent = '',
  level,
  labelType,
  rejectNull
) => {
  const indentLocal = ' '.repeat((level - 1) * 2);
  const style = styles[key]
    ? styles[key][lang]
    : { value_style: '**', title_style: '', label: key, definition: '' };
  if (
    (typeof value !== 'undefined' && value !== null && value !== '') ||
    !rejectNull ||
    !style
  ) {
    if (labelType === 'HT') {
      return `\n${indentLocal}- ${style.label}: ${style.value_style}${value}${style.value_style}`;
    } else {
      return `\n${indentLocal}- ${key}: ${style.value_style}${value}${style.value_style}`;
    }
  }
};

const handleObject = (
  key,
  value,
  styles,
  lang,
  indent = '',
  level,
  labelType,
  rejectNull
) => {
  const style = styles[key]
    ? styles[key][lang]
    : { value_style: '**', title_style: '', label: key, definition: '' };
  let result = '';
  let style_section;
  let localLabel;

  if (level === 1) {
    style_section = '#';
  } else {
    style_section = '- ###';
  }
  if (labelType === 'HT') {
    localLabel = style.label;
  } else {
    localLabel = key;
  }

  result += `\n${style_section} ${localLabel}`;
  for (let subKey in value) {
    let res = '';
    res = processKeyValue(
      key + '.' + subKey,
      value[subKey],
      styles,
      lang,
      '',
      level + 1,
      labelType,
      rejectNull
    );
    if (typeof res != 'undefined') {
      result += res;
    }
  }
  return result;
};

const handleArray = (
  key,
  value,
  styles,
  lang,
  indent = '',
  level,
  labelType,
  rejectNull,
  arraySeparator = DEFAULT_ARRAY_SEPARATOR
) => {
  const style = styles[key]
    ? styles[key][lang]
    : { value_style: '**', title_style: '', label: key, definition: '' };
  let indentLocal = ' '.repeat((level - 1) * 2);
  let result = '';
  let localLabel;

  if (labelType === 'HT') {
    localLabel = style.label;
  } else {
    localLabel = key;
  }

  if (value.every((item) => typeof item === 'string')) {
    // case: contact_mail in responsible_organisation
    result += `\n${indentLocal}- ${localLabel}: ${style.value_style}${value.join(arraySeparator)}${style.value_style}`;
  } else if (value === null && !rejectNull) {
    result += `\n${indentLocal}- ${localLabel}:`;
  } else {
    value.forEach((item, index) => {
      let value = '';
      value += processKeyValue(
        index + 1,
        item,
        styles,
        lang,
        indent,
        level + 1,
        labelType,
        rejectNull
      );
      if (typeof value != 'undefined') {
        result += value;
      }
    });
  }
  return result;
};

const handleArrayOfObjects = (
  key,
  value,
  styles,
  lang,
  indent = '',
  level,
  labelType,
  rejectNull
) => {
  const style = styles[key]
    ? styles[key][lang]
    : { value_style: '**', title_style: '', label: key, definition: '' };
  let indentLocal = ' '.repeat((level - 1) * 2);
  let result = '';
  let style_section = '';
  let localLabel = '';

  if (labelType === 'HT') {
    localLabel = style.label;
  } else {
    localLabel = key;
  }

  value.forEach((item, index) => {
    if (value.length > 1) {
      // case metadata/responsible_organisation[*]
      result += `\n${indentLocal}- ### ${localLabel} [${index + 1}]`;
    } else if (item === null && !rejectNull) {
      // case related_publication, other_related_publication, published_data_identifier
      result += `\n${indentLocal}- ${localLabel} =`;
    } else {
      if (level === 1) {
        style_section = '#';
      } else {
        // case responsible_organisation, variable
        style_section = '- ###';
      }
      // case resource/date, keyword / Controlled_vocabulary / date
      result += `\n${indentLocal}${style_section} ${localLabel}`;
    }
    for (let subKey in item) {
      let value = '';
      value = processKeyValue(
        key + '.' + subKey,
        item[subKey],
        styles,
        lang,
        indent,
        level + 1,
        labelType,
        rejectNull
      );
      if (typeof value != 'undefined') {
        result += value;
      }
    }
  });
  return result;
};

const processKeyValue = (
  key,
  value,
  styles,
  lang,
  indent = '',
  level,
  labelType,
  rejectNull
) => {
  const style = styles[key]
    ? styles[key][lang]
    : { value_style: '**', title_style: '', label: key, definition: '' };
  let indentLocal = ' '.repeat((level - 1) * 2);
  let localLabel;

  if (labelType === 'HT') {
    localLabel = style.label;
  } else {
    localLabel = key;
  }

  switch (typeof value) {
    case 'undefined':
      break;
    case 'boolean':
      break;
    case 'number':
      break;
    case 'function':
      break;
    case 'symbol':
      break;
    case 'bigint':
      break;
    case 'string':
      return handleString(key, value, styles, lang, indent, level, labelType, rejectNull);
    case 'object':
      if (Array.isArray(value)) {
        if (value.length > 0 && typeof value[0] === 'object') {
          return handleArrayOfObjects(
            key,
            value,
            styles,
            lang,
            indent,
            level,
            labelType,
            rejectNull
          );
        } else {
          return handleArray(
            key,
            value,
            styles,
            lang,
            indent,
            level,
            labelType,
            rejectNull
          );
        }
      } else if (value !== null) {
        return handleObject(
          key,
          value,
          styles,
          lang,
          indent,
          level,
          labelType,
          rejectNull
        );
      } else if ((typeof value !== 'undefined' && false && value !== '') || !rejectNull) {
        return `\n${indentLocal}- ${localLabel} = `;
      } else {
        // No value, ignored
        return '';
      }
    default:
      if (
        (typeof value !== 'undefined' && value !== null && value !== '') ||
        !rejectNull
      ) {
        return `\n${indentLocal}- ${localLabel}: ${value}`;
      }
      break;
  }
};

/*
  json: JSON data to convert to markdown.
  lang: Language for displayed labels.
  labelType: Choice for label display. Either 'ST' for standard label, or 'HT' for a human-readable label.
  rejectNull: Boolean to choose to display null variables or not.
 */
export const JSON2MD = (json, lang, labelType, rejectNull) => {
  const styles = standard_markdown_styles;
  let markdown = '';

  for (let key in json) {
    markdown += processKeyValue(
      key,
      json[key],
      styles,
      lang,
      '',
      1,
      labelType,
      rejectNull
    );
  }
  return markdown;
};
