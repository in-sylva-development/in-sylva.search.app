import React from 'react';
import { EuiText } from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import ReactJson from '@microlink/react-json-view';

const ResourceJSON = ({ resourceData }) => {
  const { t } = useTranslation('results');

  return (
    <EuiText size="s">
      <ReactJson
        src={resourceData}
        name={t('results:flyout.JSON.title')}
        iconStyle={'triangle'}
        quotesOnKeys={false}
        displayArrayKey={false}
        displayDataTypes={false}
        displayObjectSize={false}
        collapsed={false}
      />
    </EuiText>
  );
};

export default ResourceJSON;
