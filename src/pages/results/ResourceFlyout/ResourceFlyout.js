import React, { useMemo, useState } from 'react';
import {
  EuiFlyout,
  EuiFlyoutBody,
  EuiTabs,
  EuiTab,
  EuiFlyoutHeader,
  EuiText,
  EuiTitle,
  EuiSpacer,
} from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import ResourceJSON from './ResourceJSON';
import ResourceMarkdown from './ResourceMarkdown';

/*
  Flyout header text. Displays some context for the resource.
  It displays, if existing: resource title, lineage, related network, and studied factor description.
  WARNING: this makes the search tool standard dependant, because displayed variables are chosen arbitrarily and hard-coded.
 */
const HeaderText = ({ data }) => {
  return (
    <>
      <EuiTitle size="m">
        <h2>{data?.resource?.title}</h2>
      </EuiTitle>
      <EuiSpacer size="s" />
      <EuiText color="subdued">
        <p>
          {data?.resource?.lineage}
          {' - '}
          {data?.context?.related_experimental_network_title}
        </p>
      </EuiText>
      <EuiSpacer size="s" />
      <EuiText color="subdued">
        <p>{data?.studied_factor?.description}</p>
      </EuiText>
    </>
  );
};

const ResourceFlyout = ({
  resourceFlyoutData,
  setResourceFlyoutData,
  isResourceFlyoutOpen,
  setIsResourceFlyoutOpen,
}) => {
  const { t } = useTranslation('results');
  const [selectedTabId, setSelectedTabId] = useState('0');

  const closeResourceFlyout = () => {
    setResourceFlyoutData({});
    setIsResourceFlyoutOpen(false);
  };

  const resourceDataTabs = [
    {
      id: '0',
      name: 'Markdown',
      content: <ResourceMarkdown resourceData={resourceFlyoutData} />,
    },
    {
      id: '1',
      name: 'JSON',
      content: <ResourceJSON resourceData={resourceFlyoutData} />,
    },
  ];

  // Update tab content on tab selection or on flyout open
  const selectedTabContent = useMemo(() => {
    return resourceDataTabs.find((tab) => tab.id === selectedTabId)?.content;
  }, [selectedTabId, isResourceFlyoutOpen]);

  const renderTabs = () => {
    return resourceDataTabs.map((tab, index) => (
      <EuiTab
        onClick={() => setSelectedTabId(tab.id)}
        isSelected={tab.id === selectedTabId}
        key={index}
      >
        {tab.name}
      </EuiTab>
    ));
  };

  return (
    isResourceFlyoutOpen && (
      <EuiFlyout
        onClose={() => closeResourceFlyout()}
        aria-labelledby={t('results:flyout.label')}
      >
        <EuiFlyoutHeader>
          <HeaderText data={resourceFlyoutData} />
          <EuiTabs>{renderTabs()}</EuiTabs>
        </EuiFlyoutHeader>
        <EuiFlyoutBody>{selectedTabContent}</EuiFlyoutBody>
      </EuiFlyout>
    )
  );
};

export default ResourceFlyout;
