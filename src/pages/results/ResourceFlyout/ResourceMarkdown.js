import React from 'react';
import { EuiText } from '@elastic/eui';
import { JSON2MD } from './JSON2MD';
import { useTranslation } from 'react-i18next';
import Markdown from 'react-markdown';

const ResourceMarkdown = ({ resourceData }) => {
  const { i18n } = useTranslation();

  const buildMarkdown = () => {
    return JSON2MD(resourceData, i18n.language, 'HT', true);
  };

  const markdownString = buildMarkdown();

  return (
    <EuiText size="s">
      <Markdown>{markdownString}</Markdown>
    </EuiText>
  );
};

export default ResourceMarkdown;
