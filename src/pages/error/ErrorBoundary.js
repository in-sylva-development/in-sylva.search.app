import React from 'react';
import { EuiFlexGroup, EuiPage, EuiTitle } from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import { useRouteError } from 'react-router-dom';

const ErrorBoundary = () => {
  const { t } = useTranslation('common');
  const error = useRouteError();
  console.error(error);

  return (
    <EuiPage grow={true} style={{ height: '100vh' }}>
      <EuiFlexGroup alignItems={'center'} justifyContent={'center'} direction={'column'}>
        <EuiTitle size={'l'}>
          <h2>{t('common:errorPage.title')}</h2>
        </EuiTitle>
        <EuiTitle size={'s'}>
          <h2>{t('common:errorPage.reload')}</h2>
        </EuiTitle>
      </EuiFlexGroup>
    </EuiPage>
  );
};

export default ErrorBoundary;
