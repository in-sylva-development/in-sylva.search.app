import React from 'react';
import { useTranslation } from 'react-i18next';
import { EuiTitle, EuiSpacer } from '@elastic/eui';

const Home = () => {
  const { t } = useTranslation('home');

  return (
    <>
      <EuiTitle size="m">
        <h4>{t('home:pageTitle')}</h4>
      </EuiTitle>
      <EuiSpacer size={'l'} />
      <p>{t('home:searchToolDescription.part1')}</p>
      <br />
      <p>{t('home:searchToolDescription.part2')}</p>
      <br />
      <p>
        {t('home:searchToolDescription.standardLink')}
        <a
          href={
            'https://entrepot.recherche.data.gouv.fr/file.xhtml?persistentId=doi:10.15454/ELXRGY/NCVTVR&version=5.1'
          }
          target="_blank"
          rel="noopener noreferrer"
        >
          https://entrepot.recherche.data.gouv.fr/file.xhtml?persistentId=doi:10.15454/ELXRGY/NCVTVR&version=5.1
        </a>
      </p>
      <br />
      <p>{t('home:searchToolDescription.part3')}</p>
      <br />
      <p>{t('home:searchToolDescription.part4')}</p>
      <br />
      <p>{t('home:searchToolDescription.part5')}</p>
      <br />
      <p>{t('home:searchToolDescription.part6')}</p>
    </>
  );
};

export default Home;
