import React, { Suspense } from 'react';
import { createRoot } from 'react-dom/client';
import { AuthProvider } from 'oidc-react';
import App from './App';
import './i18n';
import Loading from './components/Loading';
import { EuiProvider } from '@elastic/eui';
import '@elastic/eui/dist/eui_theme_light.css';
import { GatekeeperProvider } from './contexts/GatekeeperContext';
import { TokenProvider } from './contexts/TokenContext';

const root = createRoot(document.getElementById('root'));
root.render(
  <AuthProvider
    authority={`${process.env.REACT_APP_KEYCLOAK_BASE_URL}`}
    clientId={`${process.env.REACT_APP_KEYCLOAK_CLIENT_ID}`}
    clientSecret={`${process.env.REACT_APP_KEYCLOAK_CLIENT_SECRET}`}
    redirectUri={`${process.env.REACT_APP_BASE_URL}`}
    onBeforeSignIn={() => {
      const redirectUri = window.location.hash;
      return { postLoginRedirect: redirectUri };
    }}
    onSignIn={async (user) => {
      const postLoginRedirect = user.state?.postLoginRedirect;
      window.location.href = process.env.REACT_APP_BASE_URL + '/' + postLoginRedirect;
    }}
  >
    <TokenProvider>
      <GatekeeperProvider>
        <Suspense fallback={<Loading />}>
          <EuiProvider colorMode={'light'}>
            <App />
          </EuiProvider>
        </Suspense>
      </GatekeeperProvider>
    </TokenProvider>
  </AuthProvider>
);
