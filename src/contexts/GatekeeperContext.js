import React, { createContext, useContext } from 'react';
import { InSylvaGatekeeperClient } from '../services/GatekeeperService';
import { useUserInfo } from './TokenContext';

const GatekeeperContext = createContext(null);

export const useGatekeeper = () => {
  const context = useContext(GatekeeperContext);
  return context;
};

export const GatekeeperProvider = ({ children }) => {
  const getUserInfo = useUserInfo();
  const client = new InSylvaGatekeeperClient(getUserInfo);

  return (
    <GatekeeperContext.Provider value={client}>{children}</GatekeeperContext.Provider>
  );
};
