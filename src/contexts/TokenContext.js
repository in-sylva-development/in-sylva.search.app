import React, { createContext, useContext } from 'react';
import { useAuth } from 'oidc-react';
import TokenService from '../services/TokenService';

const TokenContext = createContext(null);

export const useUserInfo = () => {
  const context = useContext(TokenContext);
  return context;
};

export const TokenProvider = ({ children }) => {
  const auth = useAuth();

  const getUserInfo = async () => {
    //console.log('Getting access token');
    if (!auth) {
      //console.log('Auth not found');
      return null;
    }

    if (auth.isLoading) {
      //console.log('Auth is loading');
      return null;
    }

    if (!auth.userData || !auth.userData.access_token) {
      //console.log('User data not found');
      await auth.signOutRedirect();
    }

    const access_token = auth.userData.access_token;
    //console.log(access_token);

    let userInfo = await new TokenService().getUserInfo(access_token);
    if (!userInfo) {
      //console.log('User info not found');
      const refreshed = await new TokenService().refreshToken(
        auth.userData.refresh_token
      );
      if (!refreshed) {
        //console.log('Error refreshing token');
        await auth.signOutRedirect();
      }
      userInfo = await new TokenService().getUserInfo(refreshed.access_token);
    }
    //console.log('User info:', userInfo);
    return { ...userInfo, ...auth.userData };
  };

  return <TokenContext.Provider value={getUserInfo}>{children}</TokenContext.Provider>;
};
