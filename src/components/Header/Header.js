import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  EuiHeader,
  EuiHeaderSection,
  EuiHeaderSectionItem,
  EuiHeaderLinks,
  EuiHeaderLink,
} from '@elastic/eui';
import HeaderUserMenu from './HeaderUserMenu';
import style from './styles';
import logoInSylva from '../../assets/favicon.svg';
import { useTranslation } from 'react-i18next';
import LanguageSwitcher from '../LanguageSwitcher/LanguageSwitcher';

const routes = [
  {
    id: 0,
    label: 'home',
    href: '/',
  },
  {
    id: 1,
    label: 'search',
    href: '/search',
  },
];

const Header = () => {
  const { t } = useTranslation(['header', 'common']);

  return (
    <EuiHeader>
      <EuiHeaderSection grow={true}>
        <EuiHeaderSectionItem>
          <img style={style.logo} src={logoInSylva} alt={t('common:inSylvaLogoAlt')} />
        </EuiHeaderSectionItem>
        <EuiHeaderLinks>
          {routes.map((link) => (
            <EuiHeaderLink key={link.id}>
              <NavLink to={link.href}>{t(`tabs.${link.label}`)}</NavLink>
            </EuiHeaderLink>
          ))}
        </EuiHeaderLinks>
      </EuiHeaderSection>
      <EuiHeaderSection side="right">
        <EuiHeaderSectionItem style={style.languageSwitcherItem} border={'none'}>
          <LanguageSwitcher />
        </EuiHeaderSectionItem>
        <EuiHeaderSectionItem style={style.userMenuItem} border={'none'}>
          <HeaderUserMenu />
        </EuiHeaderSectionItem>
      </EuiHeaderSection>
    </EuiHeader>
  );
};

export default Header;
