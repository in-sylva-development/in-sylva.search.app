import React, { useEffect, useState } from 'react';
import { useAuth } from 'oidc-react';
import {
  EuiAvatar,
  EuiFlexGroup,
  EuiFlexItem,
  EuiText,
  EuiSpacer,
  EuiPopover,
  EuiButtonIcon,
} from '@elastic/eui';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

const HeaderUserMenu = () => {
  const auth = useAuth();
  const { t } = useTranslation('header');
  const [isOpen, setIsOpen] = useState(false);
  const [username, setUsername] = useState('');

  const onMenuButtonClick = () => {
    setIsOpen(!isOpen);
  };

  const closeMenu = () => {
    setIsOpen(false);
  };

  useEffect(() => {
    setUsername(auth.userData?.profile?.preferred_username || '');
  }, []);

  const HeaderUserButton = (
    <EuiButtonIcon
      size="s"
      onClick={onMenuButtonClick}
      iconType="user"
      title={t('header:userMenu.title')}
      aria-label={t('header:userMenu.title')}
    />
  );

  return (
    <EuiPopover
      isOpen={isOpen}
      closePopover={closeMenu}
      button={HeaderUserButton}
      anchorPosition="downRight"
      ownFocus
    >
      <EuiFlexGroup gutterSize="m" responsive={false}>
        <EuiFlexItem grow={false}>
          <EuiAvatar name={username} size="xl" />
        </EuiFlexItem>
        <EuiFlexItem>
          <EuiText>{username}</EuiText>
          <EuiSpacer size="m" />
          <EuiFlexGroup>
            <EuiFlexItem>
              <EuiFlexGroup justifyContent="spaceBetween">
                <EuiFlexItem grow={false}>
                  <NavLink to={'/profile'} onClick={() => closeMenu()}>
                    {t('header:userMenu.editProfileButton')}
                  </NavLink>
                </EuiFlexItem>
                <EuiFlexItem grow={false}>
                  <NavLink to={'/'} onClick={() => auth.signOutRedirect()}>
                    {t('header:userMenu.logOutButton')}
                  </NavLink>
                </EuiFlexItem>
              </EuiFlexGroup>
            </EuiFlexItem>
          </EuiFlexGroup>
        </EuiFlexItem>
      </EuiFlexGroup>
    </EuiPopover>
  );
};

export default HeaderUserMenu;
