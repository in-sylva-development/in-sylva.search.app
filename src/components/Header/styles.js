const headerStyle = {
  logo: {
    width: '75px',
    height: '75px',
    padding: '10px',
  },
  languageSwitcherItem: {
    margin: '10px',
  },
  userMenuItem: {
    marginRight: '10px',
  },
};

export default headerStyle;
