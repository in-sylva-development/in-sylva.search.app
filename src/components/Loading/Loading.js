import React from 'react';
import styles from './styles';

const Loading = () => {
  return (
    <div style={styles.container}>
      <h1>Loading...</h1>
    </div>
  );
};

export default Loading;
