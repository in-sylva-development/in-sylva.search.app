import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../../components/Header';
import { EuiPage, EuiPageBody, EuiPageSection } from '@elastic/eui';
import styles from './styles.js';
import { Slide, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Layout = () => {
  return (
    <EuiPage style={styles.page} restrictWidth={false}>
      <EuiPageBody>
        <ToastContainer
          position="bottom-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
          transition={Slide}
        />
        <Header />
        <EuiPageSection style={styles.pageContent} grow={true}>
          <Outlet />
        </EuiPageSection>
      </EuiPageBody>
    </EuiPage>
  );
};

export default Layout;
