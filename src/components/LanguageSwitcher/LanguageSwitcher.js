import React from 'react';
import { useTranslation } from 'react-i18next';
import styles from './styles';
import { EuiSelect } from '@elastic/eui';

const LanguageSwitcher = () => {
  const { t, i18n } = useTranslation('common');

  const options = [
    { text: t('common:languages.en'), value: 'en' },
    { text: t('common:languages.fr'), value: 'fr' },
  ];

  const changeLanguage = (newLng) => {
    i18n.changeLanguage(newLng).then();
  };

  return (
    <EuiSelect
      style={styles.select}
      options={options}
      compressed={true}
      value={i18n.resolvedLanguage}
      onChange={(e) => changeLanguage(e.target.value)}
    />
  );
};

export default LanguageSwitcher;
