import React from 'react';
import { EuiTitle, EuiSpacer } from '@elastic/eui';

const ToastMessage = ({ title, message }) => {
  return (
    <>
      <EuiTitle size={'xs'}>
        <p>{title}</p>
      </EuiTitle>
      <EuiSpacer size={'m'} />
      <p>{message}</p>
    </>
  );
};

export default ToastMessage;
