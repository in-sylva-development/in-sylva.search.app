import React from 'react';

const BulletPointList = ({ children }) => {
  // Have to style list manually because of display flex container
  return <ul style={{ listStyleType: 'disc', marginLeft: '20px' }}>{children}</ul>;
};

export default BulletPointList;
