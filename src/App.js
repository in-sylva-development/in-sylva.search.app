import React from 'react';
import {
  RouterProvider,
  createHashRouter,
  Route,
  createRoutesFromElements,
} from 'react-router-dom';
import Home from './pages/home';
import Search from './pages/search';
import Profile from './pages/profile';
import Layout from './components/Layout';
import ErrorBoundary from './pages/error/ErrorBoundary';

const App = () => {
  const router = createHashRouter(
    createRoutesFromElements(
      <>
        <Route errorElement={<ErrorBoundary />} element={<Layout />}>
          <Route index path="/" element={<Home />} />
          <Route path="/search" element={<Search />} />
          <Route path="/profile" element={<Profile />} />
        </Route>
      </>
    )
  );

  return <RouterProvider router={router} />;
};

export default App;
