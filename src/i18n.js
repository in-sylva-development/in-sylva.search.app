import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';

i18n
  .use(Backend)
  .use(initReactI18next)
  .init({
    lng: 'fr',
    fallbackLng: 'fr',
    ns: 'common',
    defaultNS: 'common',
    debug: false,
    load: 'languageOnly',
    interpolation: {
      // not needed for react as it escapes by default
      escapeValue: false,
    },
    backend: {
      loadPath:
        process.env.NODE_ENV !== 'production'
          ? '/locales/{{lng}}/{{ns}}.json'
          : `${process.env.PUBLIC_URL}/locales/{{lng}}/{{ns}}.json`,
    },
  });

export default i18n;
